using System.Collections.Generic;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;

using Wheezie.ApiTools.Domain.Extensions;

namespace Wheezie.ApiTools.Domain
{
    public class ValidationResponse
    {
        /// <summary>
        /// Valid state
        /// </summary>
        public bool Valid { get; private set; }
            = false;
        /// <summary>
        /// Modified value (if valid)
        /// </summary>
        public string ModifiedValue { get; set; }
        /// <summary>
        /// Bad field object (if invalid)
        /// </summary>
        public BadField BadField { get; private set; }

        /// <summary>
        /// Create an invalid response
        /// </summary>
        public ValidationResponse() { }

        /// <summary>
        /// Create a valid response
        /// </summary>
        /// <param name="modifiedValue">Valid field object</param>
        public ValidationResponse(string modifiedValue)
        {
            this.ModifiedValue = modifiedValue;
            this.Valid = true;
        }

        /// <summary>
        /// Create an invalid response with a badfield.
        /// </summary>
        /// <param name="field">Field name</param>
        /// <param name="error">Field error</param>
        public ValidationResponse(string field, string error)
        {
            this.BadField = new BadField(field, error);
        }
    }

    public class ValidationTools
    {
        /// <summary>
        /// Validate a string against the provided criteria
        /// </summary>
        /// <param name="badFields">List of bad fields to append bad field to (if any).</param>
        /// <param name="input">Input string to verify</param>
        /// <param name="fieldName">Field name (for badfields)</param>
        /// <param name="config">Config fields of the string</param>
        /// <param name="min">MInimum limit</param>
        /// <param name="max">Maximum limit</param>
        /// <param name="defaultRequired">Field is required by default if no config value is set</param>
        /// <returns>The ValidationResponse with the result</returns>
        public static string ValidateString(List<BadField> badFields, string input, string fieldName, IConfigurationSection config, int min, int max, bool defaultRequired = true)
        {
            int configMin = config.GetLimitedInt("Minimum", min, max, min);
            int configMax = config.GetLimitedInt("Maximum", min, max, max);
            if (configMin > configMax)
            {
                configMax += configMin;
                configMin = configMax - configMin;
                configMax -= configMin;
            }
            var result = ValidateString(input, fieldName, config.GetValue<bool>("Required", defaultRequired), configMin, configMax);
            if (!result.Valid)
                badFields.Add(result.BadField);

            return (string)result.ModifiedValue;
        }

        /// <summary>
        /// Validate a string against the provided criteria
        /// </summary>
        /// <param name="input">Input string to verify</param>
        /// <param name="fieldName">Field name (for badfields)</param>
        /// <param name="required">Required string</param>
        /// <param name="min">Minimum length</param>
        /// <param name="max">Maximum length</param>
        /// <param name="trim">Trim all prepending and tailing spaces</param>
        /// <returns>The ValidationResponse with the result</returns>
        public static ValidationResponse ValidateString(string input, string fieldName, bool required, int min, int max = int.MaxValue, bool trim = true)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                if (required)
                    return new ValidationResponse(fieldName, BadField.Required);
            }
            else
            {
                if (trim)
                    input = input.Trim();
                if (input.Length < min)
                    return new ValidationResponse(fieldName, BadField.ToShort);
                else if (input.Length > max)
                    return new ValidationResponse(fieldName, BadField.ToLong);
            }

            return new ValidationResponse(input);
        }

        /// <summary>
        /// Validate a password string with special characters and numbers.
        /// </summary>
        /// <param name="input">Input to validate</param>
        /// /// <param name="fieldName">Field name (for badfields)</param>
        /// <param name="min">Minimum length</param>
        /// <param name="numbers">Should contain numbers</param>
        /// <param name="specialCharacters">Should contain special characters</param>
        /// <returns>The ValidationResponse with the result</returns>
        public static ValidationResponse ValidatePasswordString(string input, string fieldName, int min, bool numbers = true, bool specialCharacters = true)
        {
            ValidationResponse response = ValidateString(input, fieldName, true, min, int.MaxValue, false);
            if (!response.Valid)
                return response;

            input = (string)response.ModifiedValue;

            if (numbers && !input.Any(c => char.IsDigit(c)))
                return new ValidationResponse(fieldName, BadField.RequiresDigits);

            if (specialCharacters && !input.Any(c => !char.IsLetterOrDigit(c)))
                return new ValidationResponse(fieldName, BadField.RequiresSpecials);

            return new ValidationResponse(input);
        }

        /// <summary>
        /// Validate an email string
        /// </summary>
        /// <param name="input">Email address</param>
        /// <param name="fieldName">Field name</param>
        /// <returns>The ValidationResponse with the result</returns>
        public static ValidationResponse ValidateEmailString(string input, string fieldName)
        {
            if (string.IsNullOrWhiteSpace(input))
                return new ValidationResponse(fieldName, BadField.Required);

            input = input.Trim();
            if (!Regex.IsMatch(input, Regexes.Email))
                return new ValidationResponse(fieldName, BadField.Invalid);

            return new ValidationResponse(input);
        }
    }
}