﻿namespace Wheezie.ApiTools.Domain
{
    public class PermissionAccessField
    {
        public string Permission { get; }
        public uint CanTargetId { get; }
        public PermissionAccessResult Result { get; set; }
    }
}
