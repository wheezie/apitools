namespace Wheezie.ApiTools.Domain
{
    /// <summary>
    /// A BadField error
    /// </summary>
    public partial class BadField
    {
        /// <summary>
        /// Field that is incorrectly provided
        /// </summary>
        public string Field { get; set; }
        /// <summary>
        /// Explanation for the input error
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Create a BadField instance
        /// </summary>
        /// <param name="field">incorrect field name</param>
        /// <param name="error">error type</param>
        public BadField(string field, string error)
        {
            this.Field = field;
            this.Error = error;
        }
    }
}