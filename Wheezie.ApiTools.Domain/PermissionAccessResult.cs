﻿namespace Wheezie.ApiTools.Domain
{
    public enum PermissionAccessResult
    {
        InvalidPermission,
        InvalidCanTargetId,
        Denied,
        Granted
    }
}
