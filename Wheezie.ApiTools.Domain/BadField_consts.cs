namespace Wheezie.ApiTools.Domain
{
    public partial class BadField
    {
        public const string AlreadyExists = "already_exists";
        public const string Required = "required";
        public const string Invalid = "invalid";
        public const string NotConfirmed = "not_confirmed";
        public const string NotFound = "not_found";
        public const string ToLong = "to_long";
        public const string ToShort = "to_short";
        public const string RequiresDigits = "requires_digits";
        public const string RequiresSpecials = "requires_specials";
    }
}