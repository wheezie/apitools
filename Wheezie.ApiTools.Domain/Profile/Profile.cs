using Wheezie.ApiTools.EF;

namespace Wheezie.ApiTools.Domain.Profile
{
    public class Profile
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Picture { get; set; }
        public Visibility ShowName { get; set; }
    }
}