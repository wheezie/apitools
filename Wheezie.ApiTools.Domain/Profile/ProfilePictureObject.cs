﻿namespace Wheezie.ApiTools.Domain.Profile
{
    public class ProfilePictureObject
    {
        public string Normal { get; set; }
        public string Medium { get; set; }
        public string Small { get; set; }
    }
}
