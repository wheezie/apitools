﻿namespace Wheezie.ApiTools.Domain.Album
{
    public class AlbumPictureObject
    {
        public string Original { get; set; }
        public string UHD { get; set; }
        public string FHD { get; set; }
        public string HD { get; set; }
        public string SD { get; set; }
    }
}
