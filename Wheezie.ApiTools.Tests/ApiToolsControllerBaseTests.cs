using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Xunit;

using Wheezie.ApiTools.EF.Models;

namespace Wheezie.ApiTools.Tests
{
    public partial class ApiToolsControllerBaseTests : IDisposable
    {
        private readonly IConfiguration config;
        private readonly XUnitDbContext dbContext;
        private readonly ApiToolsController controller;

        public ApiToolsControllerBaseTests()
        {
            config = new ConfigurationBuilder()
                .AddInMemoryCollection(
                    new Dictionary<string, string>()
                ).Build();
            dbContext = new XUnitDbContext();
            controller = new ApiToolsController(XUnitLogger.GetLogger<ApiToolsController>(), dbContext.DbContext, config);
        }

        [Fact]
        public void UserCanTargetRole_claim_null()
        {
            setHttpContext(new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new List<ClaimsIdentity> { new ClaimsIdentity() })
            });

            Assert.False(controller.UserCanTargetRole(1));
        }
        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        public void UserCanTargetRole_claim_empty(string claimValue)
        {
            setHttpContext(new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new List<ClaimsIdentity>
                {
                    new ClaimsIdentity(new List<Claim>
                    {
                        new Claim("API_CANTARGET", claimValue)
                    })
                })
            });

            Assert.False(controller.UserCanTargetRole(200));
        }

        [Fact]
        public void UserCanTargetRole_currentRole_null()
        {
            setGenericHttpContext();

            Assert.False(controller.UserCanTargetRole(1));
        }

        [Fact]
        public void UserCanTargetRole_currentRole_targetsNull()
        {
            this.dbContext.DbContext.Roles.Add(new Role { Id = 100, CanTargetId = null, Name = "test" });
            this.dbContext.DbContext.SaveChanges();

            setGenericHttpContext();

            Assert.False(controller.UserCanTargetRole(1));
        }


        [Fact]
        public void UserCanTargetRole_currentRole_directlyTargets()
        {
            this.dbContext.DbContext.Roles.Add(new Role { Id = 100, CanTargetId = 2, Name = "test" });
            this.dbContext.DbContext.SaveChanges();

            setGenericHttpContext();
            Assert.True(controller.UserCanTargetRole(2));
        }

        [Fact]
        public void UserCanTargetRole_currentRole_indirectlyTargets()
        {
            this.dbContext.DbContext.Roles.Add(new Role { Id = 100, CanTargetId = 101, Name = "test" });
            this.dbContext.DbContext.Roles.Add(new Role { Id = 101, CanTargetId = 102, Name = "test" });
            this.dbContext.DbContext.Roles.Add(new Role { Id = 102, CanTargetId = 2, Name = "test" });
            this.dbContext.DbContext.SaveChanges();

            setGenericHttpContext();
            Assert.True(controller.UserCanTargetRole(2));
        }

        [Fact]
        public void UserCanTargetRole_currentRole_indirectlyTargetsNull()
        {
            this.dbContext.DbContext.Roles.Add(new Role { Id = 100, CanTargetId = 101, Name = "test" });
            this.dbContext.DbContext.Roles.Add(new Role { Id = 101, CanTargetId = 102, Name = "test" });
            this.dbContext.DbContext.Roles.Add(new Role { Id = 102, CanTargetId = null, Name = "test" });
            this.dbContext.DbContext.SaveChanges();

            setGenericHttpContext();
            Assert.False(controller.UserCanTargetRole(2));
        }

        private async void setHttpContext(HttpContext context)
        {
            controller.ControllerContext = new ControllerContext
            {
                HttpContext = context
            };

            await controller.RetrieveRoles();
        }
        private void setGenericHttpContext()
        {
            setHttpContext(new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new List<ClaimsIdentity>
                {
                    new ClaimsIdentity(new List<Claim>
                    {
                        new Claim("API_CANTARGET", "100")
                    })
                })
            });
        }


        public void Dispose()
        {
            dbContext.Dispose();
        }
    }
}