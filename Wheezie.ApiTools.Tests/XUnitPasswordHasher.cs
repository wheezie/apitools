using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace Wheezie.ApiTools.Tests
{
    public class XUnitPasswordHasher
    {
        public static PasswordHasher GetHasher()
        {
            var dict = new Dictionary<string, string>
            {
                { "ApiTools:Security:Password:Factor", "4" },
                { "ApiTools:Security:Password:Enhanced", "false" },
                { "ApiTools:Security:Password:Algorithm", "SHA256" },
            };

            var config = new ConfigurationBuilder()
                .AddInMemoryCollection(dict)
                .Build();
            return new PasswordHasher(config);
        }
    }
}