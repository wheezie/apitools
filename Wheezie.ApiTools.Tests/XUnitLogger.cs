using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace Wheezie.ApiTools.Tests
{
    public class XUnitLogger
    {
        public static ILogger<T> GetLogger<T>()
        {
            return new NullLogger<T>();
        }
    }
}