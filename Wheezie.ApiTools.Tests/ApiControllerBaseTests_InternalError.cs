using System;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Wheezie.ApiTools.Tests
{
    public partial class ApiControllerBaseTests
    {
        [Fact]
        public void InternalError_noObject()
        {
            Assert.IsType<StatusCodeResult>(_controllerBase.InternalError());
        }

        [Fact]
        public void InternalError_object()
        {
            var result = Assert.IsType<ObjectResult>(_controllerBase.InternalError(new { Yes = true }));
            Assert.Equal(500, result.StatusCode);
        }
    }
}