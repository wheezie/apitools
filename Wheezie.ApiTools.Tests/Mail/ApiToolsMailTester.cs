using System.Threading;
using System.Threading.Tasks;

using MimeKit;

using Wheezie.ApiTools.Mail;

namespace Wheezie.ApiTools.Tests.Mail
{
    public class ApiToolsMailTester : IApiToolsMail
    {
        private bool returnValue = true;

        public void SetReturnValue(bool value)
        {
            this.returnValue = value;
        }

        public async Task<bool> SendAsync(MimeMessage message, CancellationToken _)
            => await Task.FromResult(returnValue);

        public async Task<bool> SendAsync(string subject, TextPart body, string sender, string receiver, CancellationToken _)
            => await Task.FromResult(returnValue);

        public async Task<bool> SendAsync(string subject, string body, string sender, string receiver, CancellationToken _)
            => await Task.FromResult(returnValue);
    }
}