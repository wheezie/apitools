using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace Wheezie.ApiTools.Tests
{
    public class IConfigurationExtensionsTests
    {
        [Theory]
        [InlineData(3, 31)]
        [InlineData(6, 28)]
        [InlineData(10, 12)]
        public void GetLimitedInt_Uncorrected(int min, int max)
        {
            var dict = new Dictionary<string, string>
            {
                { "Testing:Minimum", min.ToString() },
                { "Testing:Maximum", max.ToString() },
            };

            var config = new ConfigurationBuilder()
                .AddInMemoryCollection(dict)
                .Build();
            Assert.Equal(min, config.GetLimitedInt("Testing:Minimum", 2, 32));
            Assert.Equal(max, config.GetLimitedInt("Testing:Maximum", 2, 32));
        }

        [Theory]
        [InlineData(0, 34)]
        [InlineData(1, 33)]
        public void GetLimitedInt_Corrected(int min, int max)
        {
            var dict = new Dictionary<string, string>
            {
                { "Testing:Minimum", min.ToString() },
                { "Testing:Maximum", max.ToString() },
            };

            var config = new ConfigurationBuilder()
                .AddInMemoryCollection(dict)
                .Build();
            Assert.Equal(2, config.GetLimitedInt("Testing:Minimum", 2, 32));
            Assert.Equal(32, config.GetLimitedInt("Testing:Maximum", 2, 32));
        }

        [Fact]
        public void GetLimitedInt_Default_Uncorrected()
        {
            var config = new ConfigurationBuilder()
                .Build();
            int value = config.GetLimitedInt("Nonexistant", 2, 32, 4);
            Assert.Equal(4, value);
        }

        [Fact]
        public void GetLimitedInt_Default_Corrected()
        {
            var config = new ConfigurationBuilder()
                .Build();
            int value = config.GetLimitedInt("Nonexistant", 2, 32, 1);
            Assert.Equal(2, value);
        }
    }
}