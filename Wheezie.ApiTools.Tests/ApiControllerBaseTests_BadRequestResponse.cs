using System;
using Microsoft.AspNetCore.Mvc;
using Xunit;

using Wheezie.ApiTools.Models;

namespace Wheezie.ApiTools.Tests
{
    public partial class ApiControllerBaseTests
    {
        [Fact]
        public void BadRequestResponse_one_stringinputs()
        {
            ActionResult result = _controllerBase.BadRequestResponse("test", BadField.Invalid);

            BadRequestObjectResult objectResult = Assert.IsType<BadRequestObjectResult>(result);
            BadRequestResponse badResponse = Assert.IsType<BadRequestResponse>(objectResult.Value);

            Assert.Single(badResponse.Fields);
            Assert.Equal("test", badResponse.Fields[0].Field);
            Assert.Equal(BadField.Invalid, badResponse.Fields[0].Error);
        }

        [Fact]
        public void BadRequestResponse_one_badfieldinput()
        {
            BadField field = new BadField("test", BadField.Invalid);
            ActionResult result = _controllerBase.BadRequestResponse(field);

            BadRequestObjectResult objectResult = Assert.IsType<BadRequestObjectResult>(result);
            BadRequestResponse badResponse = Assert.IsType<BadRequestResponse>(objectResult.Value);

            Assert.Single(badResponse.Fields);
            Assert.Equal(field.Field, badResponse.Fields[0].Field);
            Assert.Equal(field.Error, badResponse.Fields[0].Error);
        }

        [Fact]
        public void BadRequestResponse_many_badfield_arguments()
        {
            BadField[] badFields = new BadField[3];
            for(int i = 0; i < badFields.Length; i++)
                badFields[i] = new BadField("test" + i, BadField.Invalid);
            ActionResult result = _controllerBase.BadRequestResponse(badFields[0], badFields[1], badFields[2]);

            BadRequestObjectResult objectResult = Assert.IsType<BadRequestObjectResult>(result);
            BadRequestResponse badResponse = Assert.IsType<BadRequestResponse>(objectResult.Value);

            Assert.Equal(badFields, badResponse.Fields);
        }

        [Fact]
        public void BadRequestResponse_many_badfield_array()
        {
            BadField[] badFields = new BadField[new Random().Next(1, 10)];
            for(int i = 0; i < badFields.Length; i++)
                badFields[i] = new BadField("test" + i, BadField.Invalid);
            ActionResult result = _controllerBase.BadRequestResponse(badFields);

            BadRequestObjectResult objectResult = Assert.IsType<BadRequestObjectResult>(result);
            BadRequestResponse badResponse = Assert.IsType<BadRequestResponse>(objectResult.Value);

            Assert.Equal(badFields, badResponse.Fields);
        }
    }
}