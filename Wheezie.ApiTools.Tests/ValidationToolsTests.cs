using Xunit;

using Wheezie.ApiTools.Models;

namespace Wheezie.ApiTools.Tests
{
    public class ValidationToolsTests
    {
        [Theory]
        [InlineData(null, BadField.Required)]
        [InlineData("", BadField.Required)]
        [InlineData("  ", BadField.Required)]
        [InlineData("1", BadField.ToShort)]
        [InlineData("123", BadField.ToShort)]
        [InlineData("  123  ", BadField.ToShort)]
        [InlineData("12345678901", BadField.ToLong)]
        [InlineData("  12345678901  ", BadField.ToLong)]
        [InlineData("123456789 0", BadField.ToLong)]
        public void ValidateString_invalid(string input, string error)
        {
            var result = ValidationTools.ValidateString(input, "Test", true, 4, 10);

            Assert.False(result.Valid);
            Assert.Equal("Test", result.BadField.Field);
            Assert.Equal(error, result.BadField.Error);
        }

        [Theory]
        [InlineData("1234", "1234")]
        [InlineData("   1234   ", "1234")]
        [InlineData("34434", "34434")]
        [InlineData("1234567890", "1234567890")]
        [InlineData("   1234567890   ", "1234567890")]
        public void ValidateString_valid(string input, string expected)
        {
            var result = ValidationTools.ValidateString(input, "DoesntMatter", true, 4, 10);

            Assert.True(result.Valid);
            Assert.Equal(expected, result.ModifiedValue);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public void ValidateString_valid_notrequired(string input)
        {
            var result = ValidationTools.ValidateString(input, "Test", true, 4, 10);

            Assert.False(result.Valid);
            Assert.Equal("Test", result.BadField.Field);
            Assert.Equal(BadField.Required, result.BadField.Error);
        }

        [Theory]
        [InlineData(null, BadField.Required)]
        [InlineData("", BadField.Required)]
        [InlineData("  ", BadField.Required)]
        [InlineData("1", BadField.ToShort)]
        [InlineData("123", BadField.ToShort)]
        [InlineData("  123  ", BadField.ToShort)]
        [InlineData("asdasd", BadField.RequiresDigits, true)]
        [InlineData("asdasd##", BadField.RequiresDigits, true)]
        [InlineData("asdasd", BadField.RequiresSpecials, false, true)]
        [InlineData("asdasd123", BadField.RequiresSpecials, false, true)]
        public void ValidatePasswordString_invalid(string input, string error, bool digits = false, bool specials = false)
        {
            var result = ValidationTools.ValidatePasswordString(input, "Test", 4, digits, specials);

            Assert.False(result.Valid);
            Assert.Equal("Test", result.BadField.Field);
            Assert.Equal(error, result.BadField.Error);
        }

        [Theory]
        [InlineData("asdd", "asdd", false, false)]
        [InlineData("   asdd   ", "asdd", false, false)]
        [InlineData("asd1", "asd1", true, false)]
        [InlineData("asd#", "asd#", false, true)]
        public void ValidatePasswordString_valid(string input, string expected, bool digits, bool specials)
        {
            var result = ValidationTools.ValidatePasswordString(input, "DoesntMatter", 4, digits, specials);

            Assert.True(result.Valid);
            Assert.Equal(expected, result.ModifiedValue);
        }

        [Theory]
        [InlineData("test@domain.tld", "test@domain.tld")]
        [InlineData("   test@domain.tld   ", "test@domain.tld")]
        [InlineData("user@contoso.com", "user@contoso.com")]
        public void ValidateEmailString_valid(string input, string expected)
        {
            var result = ValidationTools.ValidateEmailString(input, "Test");

            Assert.True(result.Valid);
            Assert.Equal(expected, result.ModifiedValue);
        }

        [Theory]
        [InlineData(null, BadField.Required)]
        [InlineData("", BadField.Required)]
        [InlineData("  ", BadField.Required)]
        [InlineData("test@domain.", BadField.Invalid)]
        [InlineData("test@domain", BadField.Invalid)]
        [InlineData("test@", BadField.Invalid)]
        [InlineData("test", BadField.Invalid)]
        public void ValidateEmailString_invalid(string input, string error)
        {
            var result = ValidationTools.ValidateEmailString(input, "Test");

            Assert.False(result.Valid);
            Assert.Equal("Test", result.BadField.Field);
            Assert.Equal(error, result.BadField.Error);
        }
    }
}