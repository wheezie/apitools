using System;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

using Wheezie.ApiTools.EF;

namespace Wheezie.ApiTools.Tests
{
    public class XUnitDbContext : IDisposable
    {
        public AppDbContext DbContext { get; private set; }

        private readonly SqliteConnection _conn;
        private bool IsDisposing = false;
        public XUnitDbContext()
        {
            _conn = new SqliteConnection("DataSource=:memory:");
            _conn.Open();


            var builder = new DbContextOptionsBuilder<AppDbContext>();
            builder.UseSqlite(_conn);

            DbContext = new AppDbContext(builder.Options);
            DbContext.Database.EnsureDeleted();
            DbContext.Database.EnsureCreated();
        }

        public void Dispose()
        {
            if (IsDisposing)
                return;

            IsDisposing = true;
            _conn.Close();
        }
    }
}