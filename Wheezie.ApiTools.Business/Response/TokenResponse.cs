﻿namespace Wheezie.ApiTools.Business.Response
{
    public class TokenResponse
    {
        public string Token { get; set; }
    }
}
