﻿using Wheezie.ApiTools.Domain;

namespace Wheezie.ApiTools.Business.Response
{
    public class PermissionAccessResponse
    {
        public PermissionAccessField[] Permissions { get; }
    }
}
