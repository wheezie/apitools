using System.Collections.Generic;
using Wheezie.ApiTools.Domain;

namespace Wheezie.ApiTools.Business.Response
{
    /// <summary>
    /// A BadRequest response
    /// </summary>
    public class BadRequestResponse
    {
        /// <summary>
        /// List of bad fields
        /// </summary>
        public List<BadField> Fields { get; set; }
            = new List<BadField>();

        /// <summary>
        /// Create an empty BadRequest response
        /// </summary>
        public BadRequestResponse() {}

        /// <summary>
        /// Create a BadRequest response with a single BadField
        /// </summary>
        /// <param name="field">Field name</param>
        /// <param name="error">Field error</param>
        public BadRequestResponse(string field, string error)
        {
            this.Fields.Add(new BadField(field, error));
        }

        /// <summary>
        /// Create a BadRequest response filled with BadFields.
        /// </summary>
        /// <param name="badFields">BadFields to fill</param>
        public BadRequestResponse(params BadField[] badFields)
        {
            this.Fields.AddRange(badFields);
        }
    }
}