﻿using Microsoft.Extensions.Configuration;

using Wheezie.ApiTools.Business.Extensions;
using Wheezie.ApiTools.Domain.Profile;
using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.EF.Models;

namespace Wheezie.ApiTools.Business.Response
{
    public class AccountResponse
    {
        public ulong Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public ProfilePictureObject Picture { get; set; }

        public Visibility ShowWebsite { get; set; }
        public Visibility ShowEmail { get; set; }
        public Visibility ShowName { get; set; }

        public AccountResponse() { }
        public AccountResponse(Account account, IConfigurationSection config)
        {
            Id = account.Id;
            FirstName = account.FirstName;
            LastName = account.LastName;
            Username = account.Username;
            Email = account.ProfileEmail?.Email ?? "";
            Picture = account.PictureId.AsProfilePicture(config);

            ShowWebsite = account.ShowWebsite;
            ShowEmail = account.ShowEmail;
            ShowName = account.ShowName;
        }
    }
}
