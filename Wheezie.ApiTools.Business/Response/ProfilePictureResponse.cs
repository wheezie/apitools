﻿using Wheezie.ApiTools.Domain.Profile;

namespace Wheezie.ApiTools.Business.Response
{
    public class ProfilePictureResponse : PictureResponse<ProfilePictureObject> { }
}
