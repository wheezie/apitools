﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Extensions.Configuration;

using Wheezie.ApiTools.EF.Models;

namespace Wheezie.ApiTools.Business.Response
{
    public class AccountRoleResponse : AccountResponse
    {
        public Role Role { get; set; }

        public AccountRoleResponse() { }
        public AccountRoleResponse(Account account, IConfigurationSection config) : base(account, config)
        {
            Role = account.Role;
        }
    }
}
