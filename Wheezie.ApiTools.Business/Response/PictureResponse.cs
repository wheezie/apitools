﻿using System;

namespace Wheezie.ApiTools.Business.Response
{
    public class PictureResponse<T>
    {
        public Guid Id { get; set; }
        public T Picture { get; set; }
        public DateTime Uploaded { get; set; }
    }
}
