﻿using Microsoft.Extensions.Configuration;

using Wheezie.ApiTools.Business.Extensions;
using Wheezie.ApiTools.Domain.Album;
using Wheezie.ApiTools.EF.Models;

namespace Wheezie.ApiTools.Business.Response
{
    public class AlbumPictureResponse : PictureResponse<AlbumPictureObject>
    {
        public AlbumPictureResponse() {}
        public AlbumPictureResponse(Picture picture, IConfigurationSection config)
        {
            Id = picture.Id;
            Picture = picture.Id.AsAlbumPicture(config);
            Uploaded = picture.Uploaded;
        }
    }
}
