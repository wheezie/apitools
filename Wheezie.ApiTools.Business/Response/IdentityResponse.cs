﻿using System;

namespace Wheezie.ApiTools.Business.Response
{
    public class IdentityResponse : TokenResponse
    {
        public AccountRoleResponse Identity { get; set; }

        public IdentityResponse(Guid token, AccountRoleResponse identity)
        {
            Token = token.ToString("N");
            Identity = identity;
        }
    }
}
