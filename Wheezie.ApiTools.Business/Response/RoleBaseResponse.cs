﻿namespace Wheezie.ApiTools.Business.Response
{
    public class RoleBaseResponse
    {
        public uint Id { get; set; }
        public uint? CanTargetId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
