﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheezie.ApiTools.Business.Response
{
    public class AlbumResponse
    {
        public uint Id { get; set; }
        public string Description { get; set; }
        public AccountResponse Account { get; set; }
        public long Likes { get; set; }
        public IEnumerable<AlbumPictureResponse> Pictures { get; set; }
    }
}
