using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.Business.Extensions;

namespace Wheezie.ApiTools.Business.Authorization
{
    public class ApiAuthorizationHandler : AuthorizationHandler<ApiPermissionRequirement>
    {
        private readonly AppDbContext _dbContext;

        public ApiAuthorizationHandler(AppDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, ApiPermissionRequirement requirement)
        {
            uint? roleId = context.User.GetRoleId();

            if (requirement.RequiresAuthentication && context.User.GetAccountId() == 0)
                return;

            // Just requires authorization, no specific permission
            if (string.IsNullOrWhiteSpace(requirement.Permission)) {
                context.Succeed(requirement);
                return;
            }

            var permission = await this._dbContext.RolePermissions
                .Include(p => p.Role)
                .Where(p => p.RoleId == roleId && !p.Role.Disabled && (p.Permission == requirement.Permission || p.Permission == "*"))
                .Select(p => new
                {
                    CanTargetId = p.CanTargetId ?? p.Role.CanTargetId
                })
                .FirstOrDefaultAsync();

            if (permission != null)
            {
                context.User.AddIdentity(new ClaimsIdentity(new Claim[] { new Claim("API_CANTARGET", permission.CanTargetId?.ToString() ?? "") }));
                context.Succeed(requirement);
            }
        }
    }
}