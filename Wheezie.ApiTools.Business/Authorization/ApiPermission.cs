using Microsoft.AspNetCore.Authorization;

namespace Wheezie.ApiTools.Business.Authorization
{

    public class ApiPermissionAttribute : AuthorizeAttribute
    {
        public ApiPermissionAttribute(bool requiresAuthentication = true)
                => Policy = $"{ApiAuthPolicyProvider.POLICY_PREFIX}${requiresAuthentication.ToString()}";
        public ApiPermissionAttribute(string permission, bool requiresAuthentication = true)
                => Policy = $"{ApiAuthPolicyProvider.POLICY_PREFIX}{permission}${requiresAuthentication.ToString()}";
    }

    public class ApiPermissionRequirement : IAuthorizationRequirement
    {
        public readonly string Permission;
        public readonly bool RequiresAuthentication;

        public ApiPermissionRequirement(string policy)
        {
            Permission = policy.Substring(
                ApiAuthPolicyProvider.POLICY_PREFIX.Length,
                (policy.IndexOf('$') - ApiAuthPolicyProvider.POLICY_PREFIX.Length));
            RequiresAuthentication = System.Convert.ToBoolean(
                policy.Substring(policy.IndexOf('$') + 1));
        }
    }
}