using System;
using Microsoft.Extensions.Configuration;

using Wheezie.ApiTools.Business.Pictures;

using Wheezie.ApiTools.Domain.Album;
using Wheezie.ApiTools.Domain.Profile;

namespace Wheezie.ApiTools.Business.Extensions
{
    public static class GuidExtensions
    {
        public static ProfilePictureObject AsProfilePicture(this Guid? pictureId, IConfigurationSection config)
        {
            if (!pictureId.HasValue)
                return null;

            return pictureId.Value.AsProfilePicture(config);
        }

        public static ProfilePictureObject AsProfilePicture(this Guid pictureId, IConfigurationSection config)
        {
            string path = $"{config.GetValue<string>("Paths:Access:Pictures")}{pictureId.ToString("N")}";
            return new ProfilePictureObject
            {
                Normal = PictureFormat.PROFILE_PICTURES[0].GetPath(path),
                Medium = PictureFormat.PROFILE_PICTURES[1].GetPath(path),
                Small = PictureFormat.PROFILE_PICTURES[2].GetPath(path)
            };
        }

        public static AlbumPictureObject AsAlbumPicture(this Guid pictureId, IConfigurationSection config)
        {
            string path = $"{config.GetValue<string>("Paths:Access:Pictures")}{pictureId.ToString("N")}";
            return new AlbumPictureObject
            {
                Original = PictureFormat.PICTURES[0].GetPath(path),
                UHD = PictureFormat.PICTURES[1].GetPath(path),
                FHD = PictureFormat.PICTURES[2].GetPath(path),
                HD = PictureFormat.PICTURES[3].GetPath(path),
                SD = PictureFormat.PICTURES[4].GetPath(path)
            };
        }
    }
}