using System;
using System.Linq;
using System.Security.Claims;

namespace Wheezie.ApiTools.Business.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        /// <summary>
        /// Retrieve the account identifier
        /// </summary>
        public static ulong GetAccountId(this ClaimsPrincipal principal)
            => Convert.ToUInt64(principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid)?.Value);
        /// <summary>
        /// Retrieve the account identifier
        /// </summary>
        public static uint? GetRoleId(this ClaimsPrincipal principal)
        {
            var claim = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            if (claim == null || string.IsNullOrWhiteSpace(claim.Value))
                return null;
            return Convert.ToUInt32(claim.Value);
        }

        /// <summary>
        /// Retrieve the permission's targettable group
        /// </summary>
        public static uint? GetPermissionCanTarget(this ClaimsPrincipal principal)
        {
            var claim = principal.Claims.FirstOrDefault(c => c.Type == "API_CANTARGET");
            if (claim == null || string.IsNullOrWhiteSpace(claim.Value))
                return null;
            return Convert.ToUInt32(claim.Value);
        }
        /// <summary>
        /// Retrieve the session identifier
        /// </summary>
        public static Guid GetSessionToken(this ClaimsPrincipal principal)
            => Guid.Parse(principal.Claims.FirstOrDefault(c => c.Type == "API_TOKEN").Value);
    }
}