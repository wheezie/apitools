using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Processing;
using System.IO;

namespace Wheezie.ApiTools.Business.Extensions
{
    public static class ImageExtensions
    {
        private static readonly JpegEncoder ENCODER = new JpegEncoder
        {
            Quality = 85
        };

        public static Image FitResolution(this Image image, ResizeOptions options)
            => image?.Clone(i => i.Resize(options));

        public static void SaveToDisk(this Image image, string imagePath)
        {
            using (FileStream fileStream = File.OpenWrite(imagePath))
                image.SaveAsJpeg(fileStream, ENCODER);
        }
    }
}