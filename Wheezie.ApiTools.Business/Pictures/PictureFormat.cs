using System;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace Wheezie.ApiTools.Business.Pictures
{
    public class PictureFormat
    {
        public static readonly PictureFormat[] PICTURES = new PictureFormat[]
        {
            new PictureFormat(0,0),
            new PictureFormat(3840, 0, "_UHD"),
            new PictureFormat(1920, 0, "_FHD"),
            new PictureFormat(1280, 0, "_HD"),
            new PictureFormat(872, 0, "_SD")
        };

        public static readonly PictureFormat[] PROFILE_PICTURES = new PictureFormat[]
        {
            new PictureFormat(400, 400),
            new PictureFormat(128, 128, "_128"),
            new PictureFormat(48, 48, "_48")
        };


        public string Suffix { get; set; }
        public string Extension { get; set; }
                = "jpg";
        public ResizeOptions Options { get; private set; }
                = new ResizeOptions
                {
                    Mode = ResizeMode.Crop,
                    Position = AnchorPositionMode.Center
                };

        public PictureFormat(int width, int height, string suffix = null)
        {
            Options.Size = new Size(width, height);
            Suffix = suffix;
        }

        public string GetPath(string imagePath)
            => $"{imagePath}{Suffix}.{Extension}";
    }
}