using System;

using Wheezie.ApiTools.EF.Models;
using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.Business.Request
{
    public class EventRequest
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Event name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Event description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Promotion url
        /// </summary>
        public string Promotion { get; set; }
        /// <summary>
        /// Promotion type
        /// </summary>
        public ContentType PromotionType { get; set; }
        /// <summary>
        /// Account identifier
        /// </summary>
        public ulong?  AccountId { get; set; }
        /// <summary>
        /// Dates
        /// </summary>
        public DateTime[] Dates { get; set; }
        /// <summary>
        /// Members
        /// </summary>
        public EventCrew[] Crews { get; set; }
    }
}