﻿using Wheezie.ApiTools.Domain;

namespace Wheezie.ApiTools.Business.Request
{
    public class PermissionAccessRequest
    {
        public PermissionAccessField[] Permissions { get; set; }
    }
}
