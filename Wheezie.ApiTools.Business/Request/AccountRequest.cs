using Wheezie.ApiTools.EF.Models;

namespace Wheezie.ApiTools.Business.Request
{
    public class AccountRequest
    {
        public ulong Id { get; set; }
        public AccountState State { get; set; }
    }
}