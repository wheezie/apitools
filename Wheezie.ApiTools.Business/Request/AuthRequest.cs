namespace Wheezie.ApiTools.Business.Request
{
    public class AuthBaseRequest
    {

        /// <summary>
        /// Account password
        /// </summary>
        public string Password { get; set; }
    }

    public class AuthRequest : AuthBaseRequest
    {
        /// <summary>
        /// Username/Email
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Account's new password
        /// </summary>
        public string NewPassword { get; set; }
    }
}