using System.Collections.Generic;
using System;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;

using Isopoh.Cryptography.Argon2;
using System.Security.Cryptography;
using System.Text;
using Wheezie.ApiTools.Domain.Extensions;
using System.Threading;

namespace Wheezie.ApiTools.Business.Authentication
{
    public class PasswordHasher
    {
        private readonly int _factor, _memory, _threads;

        /// <summary>
        /// Api Password hashing wrapper
        /// </summary>
        /// <param name="config">(DI) configuration file</param>
        public PasswordHasher(IConfiguration config)
        {
            config = config.GetSection("ApiTools:Security:Password");
            _factor = config.GetValue("Factor", 12);
            _memory = config.GetLimitedInt("Memory", Argon2.BlockSize, (Argon2.BlockSize * 4096), 65536);
            _threads = config.GetLimitedInt("Threads", 1, Environment.ProcessorCount, 1);
        }

        /// <summary>
        /// BCrypt hash the provided password
        /// </summary>
        /// <param name="password">Input password</param>
        /// <returns>Generated password hash</returns>
        public string GenerateHash(string password)
            => Argon2.Hash(password, _factor, _memory, _threads, Argon2Type.HybridAddressing);

        /// <summary>
        /// Verify the password against the hash.
        /// </summary>
        /// <param name="password">Input password</param>
        /// <param name="hashPassword">Password hash</param>
        /// <returns>Validity of the password</returns>
        public bool Verify(string password, ref string hashPassword, out bool updated)
        {
            if (Argon2.Verify(hashPassword, password))
            {
                //Argon2.
                //if ()
                //hashPassword = ;

                //return true;
            }

            updated = false;
            return false;
        }

        /// <summary>
        /// Benchmark bcrypt performance using your hardware.
        /// </summary>
        /// <param name="maxFactor">Maximum factor to start benchmarking from</param>
        /// <param name="minFactor">Factor to stop benchmarking after</param>
        /// <returns>List of benchmark results</returns>
        public Dictionary<int, TimeSpan> BenchmarkFactor(int maxFactor = 20, int minFactor = 4)
        {
            Dictionary<int, TimeSpan> result = new Dictionary<int, TimeSpan>();
            Stopwatch sw = Stopwatch.StartNew();
            for(int factor = maxFactor; factor > minFactor; factor--)
            {
                StringBuilder stringBuilder = new StringBuilder(32);
                for (int i = 0; i < 32; i++)
                {
                    stringBuilder.Append((char)i);
                    sw.Start();
                    Argon2.Hash(stringBuilder.ToString(), factor, _memory, _threads, Argon2Type.HybridAddressing);
                    sw.Stop();
                }

                result.Add(factor, sw.Elapsed.Divide(32));
                sw.Reset();
            }

            return result;
        }
    }
}