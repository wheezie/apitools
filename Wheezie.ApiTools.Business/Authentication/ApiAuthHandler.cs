using System;

using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;

using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.EF.Models;

namespace Wheezie.ApiTools.Business.Authentication
{
    public class ApiAuthHandler : AuthenticationHandler<ApiAuthOptions>
    {
        private readonly AppDbContext appDbContext;
        private readonly IConfigurationSection config;
        public ApiAuthHandler(IOptionsMonitor<ApiAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, AppDbContext dbContext, IConfiguration config)
            : base(options, logger, encoder, clock)
        {
            appDbContext = dbContext;
            this.config = config.GetSection("ApiTools:Security");
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            AuthenticationTicket ticket;
            if (Request.Headers.TryGetValue(HeaderNames.Authorization, out var authorization) && Guid.TryParse(authorization.FirstOrDefault(), out var sessionId)) {
                Account account;
                try {
                    account = await appDbContext.Sessions
                        .Include(s => s.Account)
                        .Where(s => s.Id == sessionId
                            && s.AccountId != null
                            && s.Expires > DateTime.UtcNow
                            && s.Account.State == AccountState.Confirmed)
                            .AsNoTracking()
                            .Select(s => new Account
                            {
                                Id = s.Account.Id,
                                RoleId = s.Account.RoleId,
                                State = s.Account.State
                            })
                            .FirstOrDefaultAsync();

                    if (account == null)
                        return AuthenticateResult.Fail("Couldn't find account");

                    Session session = new Session {
                        Id = sessionId
                    };
                    this.appDbContext.Attach(session);
                    session.LastIp = (this.config.GetValue<bool>("Session:LogLastIp", true)) ? Request.HttpContext.Connection.RemoteIpAddress.ToString() : null;
                    session.LastUse = DateTime.UtcNow;
                    session.Expires = DateTime.UtcNow.AddMinutes(this.config.GetValue<uint>("Session:Lifetime"));

                    await this.appDbContext.SaveChangesAsync();
                } catch {
                    return AuthenticateResult.Fail("Internal error occurred.");
                }

                var identity = new ClaimsIdentity(new Claim[]
                {
                    new Claim("API_TOKEN", sessionId.ToString()),
                    new Claim(ClaimTypes.Sid, account.Id.ToString()),
                    new Claim(ClaimTypes.Role, account.RoleId.ToString())
                }, Options.Scheme);
                ticket = new AuthenticationTicket(new ClaimsPrincipal(new List<ClaimsIdentity> { identity }), Options.Scheme);
            } else {
                var identity = new ClaimsIdentity(new Claim[]
                {
                    new Claim("API_TOKEN", ""),
                    new Claim(ClaimTypes.Sid, "0"),
                    new Claim(ClaimTypes.Role, this.config.GetValue<uint>("DefaultRole", 0).ToString())
                }, Options.Scheme);
                ticket = new AuthenticationTicket(new ClaimsPrincipal(new List<ClaimsIdentity> { identity }), Options.Scheme);
            }

            return AuthenticateResult.Success(ticket);
        }
    }
}