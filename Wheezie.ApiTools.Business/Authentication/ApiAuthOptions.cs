using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Primitives;

namespace Wheezie.ApiTools.Business.Authentication
{
    public class ApiAuthOptions : AuthenticationSchemeOptions
    {
        public const string DefaultScheme = "ApiTools";
        public string Scheme => DefaultScheme;
    }
}