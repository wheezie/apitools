using Microsoft.AspNetCore.Mvc;

using Wheezie.ApiTools.Business.Response;
using Wheezie.ApiTools.Domain;

namespace Wheezie.ApiTools
{
    public class ApiControllerBase : ControllerBase
    {
        /// <summary>
        /// Retrieve the BadRequest response with one field
        /// </summary>
        /// <param name="field">Field name</param>
        /// <param name="error">Error type</param>
        /// <returns>The response object</returns>
        [NonAction]
        public BadRequestObjectResult BadRequestResponse(string field, string error)
                => BadRequest(new BadRequestResponse(field, error));

        /// <summary>
        /// Retrieve the BadRequest response for the bad fields
        /// </summary>
        /// <param name="badFields">BadField or list of</param>
        /// <returns>The response object</returns>
        [NonAction]
        public BadRequestObjectResult BadRequestResponse(params BadField[] badFields)
                => BadRequest(new BadRequestResponse(badFields));

        /// <summary>
        /// Rerieve an objectresult for internal error responding
        /// </summary>
        /// <returns>The response object</returns>
        [NonAction]
        public StatusCodeResult InternalError()
                => StatusCode(500);
        /// <summary>
        /// Rerieve an objectresult for internal error responding
        /// </summary>
        /// <param name="content">Response body</param>
        /// <returns>The response object</returns>
        [NonAction]
        public ObjectResult InternalError(object content)
                => StatusCode(500, content);
    }
}