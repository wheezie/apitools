using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.EF.Models;
using Wheezie.ApiTools.Business.Pictures;
using SixLabors.ImageSharp;
using Wheezie.ApiTools.Business.Extensions;

namespace Wheezie.ApiTools
{
    public class ApiToolsController : ApiControllerBase
    {
        protected readonly ILogger logger;
        protected readonly AppDbContext dbContext;
        protected readonly IConfigurationSection config;
        protected List<Role> roles { get; private set; }

        protected ulong AccountId
                => HttpContext.User.GetAccountId();

        /// <summary>
        /// Extending the controller base with authentication basics
        /// </summary>
        public ApiToolsController(ILogger logger, AppDbContext dbContext, IConfigurationSection config = null)
        {
            this.logger = logger;
            this.dbContext = dbContext;
            this.config = config;
        }
        /// <summary>
        /// Extending the controller base with authentication basics
        /// </summary>
        public ApiToolsController(ILogger logger, AppDbContext dbContext, IConfiguration config = null)
            : this(logger, dbContext, config.GetSection("ApiTools")) {}

        /// <summary>
        /// Retrieve the roles from the database
        /// </summary>
        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task RetrieveRoles(bool track = false)
        {
            if (!track)
                roles = await this.dbContext.Roles.AsNoTracking().ToListAsync();
            else
                roles = await this.dbContext.Roles.ToListAsync();
        }
        /// <summary>
        /// Can the current authorized user target this role
        /// </summary>
        /// <param name="targetId">Role to check targetability for</param>
        /// <returns>Targetable state</returns>
        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public bool UserCanTargetRole(uint targetId)
        {
            uint? roleId = HttpContext.User.GetPermissionCanTarget();
            if (roleId == null)
                return false;

            return CanTargetRole(roleId.Value, targetId);
        }

        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public bool CanTargetRole(uint roleId, uint targetId)
        {
            var currentRole = roles.FirstOrDefault(r => r.Id == roleId);
            while (currentRole != null)
            {
                if (currentRole.Id == targetId)
                    return true;

                // Check null and prevent infinite looping
                if (currentRole.CanTargetId != null && currentRole.CanTargetId != currentRole.Id)
                {
                    if (currentRole.CanTargetId == targetId)
                        return true;
                    else
                        currentRole = roles.FirstOrDefault(r => r.Id == currentRole.CanTargetId);
                }
                else
                    currentRole = null;
            }

            return false;
        }

        /// <summary>
        /// Uploads and handles a picture upload
        /// </summary>
        /// <param name="file">File (picture) to save</param>
        /// <param name="pictureFormats">Formats etc</param>
        /// <param name="transaction">Transaction to add picture with</param>
        /// <returns>The picture db model if succeeded</returns>
        /// <exception cref="SixLabors.ImageSharp.UnknownImageFormatException">If no valid file format was provided</exception>
        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<Picture> UploadPicture(IFormFile file, IEnumerable<PictureFormat> pictureFormats, IDbContextTransaction transaction)
        {
            if (file == null || pictureFormats == null || pictureFormats.Count() <= 0)
                return null;

            string imagePath = config.GetValue<string>("Paths:Storage:Pictures");
            Directory.CreateDirectory(imagePath);

            using Stream stream = file.OpenReadStream();
            using Image image = Image.Load(stream);

            Picture picture = new Picture
            {
                Id = Guid.NewGuid(),
                State = PictureState.Public,
                Uploaded = DateTime.UtcNow,
                UploaderId = AccountId
            };

            imagePath += picture.Id.ToString("N");

            try {
                this.dbContext.Add(picture);
                await this.dbContext.SaveChangesAsync();

                foreach (var format in pictureFormats)
                {
                    await Task.Run(() => {
                        if (format.Options.Size.Width == 0 && format.Options.Size.Height == 0) {
                            image.SaveToDisk($"{imagePath}{format.Suffix}.{format.Extension}");
                        } else {
                            using (var formattedImage = image.FitResolution(format.Options))
                                formattedImage.SaveToDisk($"{imagePath}{format.Suffix}.{format.Extension}");
                        }
                    }).ConfigureAwait(false);
                }

            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't create picture in database. Rolling back.");
                RevertPicture(picture.Id, transaction);
                return null;
            } catch (IOException ex) {
                this.logger.LogError(ex, "Something went wrong storing images to disk for picture {PictureId}. Rolling back.", picture.Id);
                RevertPicture(picture.Id, transaction);
                return null;
            }

            return picture;
        }

        /// <summary>
        /// Remove an existing picture
        /// </summary>
        /// <param name="pictureId">Picture identifier</param>
        /// <param name="notOwned">Remove even if not uploaded themselves</param>
        /// <returns>The success</returns>
        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<bool> RemovePicture(Guid pictureId, bool notOwned = false)
        {
            try {
                if (!notOwned)
                    this.dbContext.Remove(new Picture { Id = pictureId });
                else {
                    this.dbContext.Pictures.RemoveRange(this.dbContext.Pictures.Where(a =>
                        a.UploaderId == HttpContext.User.GetAccountId() && a.Id == pictureId));
                }
                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't remove picture from database");
                return false;
            }

            DiskRemovePicture(pictureId);
            return true;
        }

        /// <summary>
        /// Revert a picture transaction and remove it from disk
        /// </summary>
        /// <param name="pictureId">Picture identifier</param>
        /// <param name="transaction">Current transaction</param>
        private void RevertPicture(Guid pictureId, IDbContextTransaction transaction)
        {
            DiskRemovePicture(pictureId);
            transaction.RollbackAsync();
        }

        /// <summary>
        /// Remove a picture from disk
        /// </summary>
        /// <param name="pictureId">Picture identifier</param>
        private void DiskRemovePicture(Guid pictureId)
        {
            foreach (var file in Directory.EnumerateFiles(config.GetValue<string>("Paths:Storage:Pictures"), $"{pictureId.ToString("N")}*.*"))
                Task.Run(() => System.IO.File.Delete(file));
        }
    }
}