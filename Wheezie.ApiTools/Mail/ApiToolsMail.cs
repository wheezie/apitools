using System.Threading;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace Wheezie.ApiTools.Mail
{
    public class ApiToolsMail : IApiToolsMail, IDisposable
    {
        private bool disposing;
        private readonly SmtpClient smtpClient;
        private readonly IConfigurationSection config;
        private readonly ILogger<ApiToolsMail> logger;

        public ApiToolsMail(IConfiguration config, ILogger<ApiToolsMail> logger)
        {
            this.smtpClient = new SmtpClient();
            this.config = config.GetSection("ApiTools:Mail");
            this.logger = logger;
        }

        /// <summary>
        /// Send a mail message over smtp
        /// </summary>
        /// <param name="message">Mime message format</param>
        /// <returns>Message send state</returns>
        public async Task<bool> SendAsync(MimeMessage message, CancellationToken cancellationToken)
        {
            try {
                await smtpClient.ConnectAsync(this.config.GetValue<string>("Smtp:Hostname"),
                    this.config.GetValue<int>("Smtp:Port"),
                    this.config.GetValue<SecureSocketOptions>("Smtp:Ssl"));
                await smtpClient.SendAsync(message, cancellationToken);
                await smtpClient.DisconnectAsync(true).ConfigureAwait(false);

                return true;
            } catch (Exception ex) {
                this.logger.LogError(ex, "Couldn't send mail {Subject} over Smtp to {Addressess}", message.Subject, message.To);
            }

            return false;
        }
        /// <summary>
        /// Send a mail message over smtp
        /// </summary>
        /// <param name="subject">Mail subject</param>
        /// <param name="body">Mail contents</param>
        /// <param name="sender">Sender mail address</param>
        /// <param name="receiver">Receiver mail address</param>
        /// <returns>Message send state</returns>
        public async Task<bool> SendAsync(string subject, TextPart body, string sender, string receiver, CancellationToken cancellationToken)
        {
            var mimeMessage = new MimeMessage
            {
                Subject = subject,
                Body = body
            };

            mimeMessage.From.Add(new MailboxAddress(sender, sender));
            mimeMessage.To.Add(new MailboxAddress(receiver, receiver));
            return await SendAsync(mimeMessage, cancellationToken);
        }
        /// <summary>
        /// Send a mail message over smtp
        /// </summary>
        /// <param name="subject">Mail subject</param>
        /// <param name="body">Mail contents</param>
        /// <param name="sender">Sender mail address</param>
        /// <param name="receiver">Receiver mail address</param>
        /// <returns>Message send state</returns>
        public async Task<bool> SendAsync(string subject, string body, string sender, string receiver, CancellationToken cancellationToken)
            => await SendAsync(subject, new TextPart("plain", body), sender, receiver, cancellationToken);

        public void Dispose()
        {
            if (disposing)
                return;

            this.disposing = true;
            this.smtpClient.Dispose();
        }
    }
}