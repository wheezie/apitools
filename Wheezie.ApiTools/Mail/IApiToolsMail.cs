using System.Threading;
using System.Threading.Tasks;

using MailKit.Net.Smtp;
using MimeKit;

namespace Wheezie.ApiTools.Mail
{
    public interface IApiToolsMail
    {
        Task<bool> SendAsync(MimeMessage message, CancellationToken cancellationToken);
        Task<bool> SendAsync(string subject, TextPart body, string sender, string receiver, CancellationToken cancellationToken);
        Task<bool> SendAsync(string subject, string body, string sender, string receiver, CancellationToken cancellationToken);
    }
}