using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Wheezie.ApiTools.Business.Authorization;
using Wheezie.ApiTools.Business.Extensions;
using Wheezie.ApiTools.Business.Request;
using Wheezie.ApiTools.Domain;
using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.EF.Models;

namespace Wheezie.ApiTools.Controllers
{
    [ApiController, Route("event")]
    public class EventController : ApiToolsController
    {
        public EventController(ILogger<ErrorController> logger, AppDbContext dbContext, IConfiguration config)
            : base(logger, dbContext, config.GetSection("ApiTools")) {}

        [HttpGet, ApiPermission("EVENT_VIEW", false), ResponseCache(Duration = 300, Location = ResponseCacheLocation.Any)]
        public async Task<ActionResult> GetEvents()
        {
            var events = await this.dbContext.Events
                .Include(e => e.Dates)
                .Select(e => new
                {
                    Id = e.Id,
                    Name = e.Name,
                    Dates = e.Dates.Select(d => d.Date)
                })
                .ToArrayAsync();
            if (events == null || events.Length <= 0)
                return NoContent();

            return Ok(events);
        }

        [HttpGet("{id}"), ApiPermission("EVENT_VIEW", false), ResponseCache(Duration = 60, Location = ResponseCacheLocation.Any)]
        public async Task<ActionResult> GetEvent(uint id)
        {
            var eventObject = await this.dbContext.Events.Where(e => e.Id == id)
                .Include(e => e.Dates)
                .Include(e => e.Crews)
                .Select(e => new
                {
                    Id = e.Id,
                    Name = e.Name,
                    Description = e.Description,
                    Dates = e.Dates.Select(d => d.Date),
                    Promotion = e.Promotion,
                    PromotionType = e.PromotionType,
                    Crews = e.Crews.Select(c => new
                    {
                        Name = c.Name,
                        Members = c.Members.Select(m => new
                        {
                            Title = m.Title,
                            Name = m.Name
                        })
                    })
                })
                .FirstOrDefaultAsync();
            if (eventObject == null)
                return NoContent();

            return Ok(eventObject);
        }

        [HttpPost, ApiPermission("EVENT_ADD")]
        public async Task<ActionResult> AddEvent([FromBody]EventRequest evnt)
        {
            var validationResult = validateEventRequest(evnt);
            if (validationResult != null)
                return validationResult;

            var newEvent = new Event
            {
                AccountId = HttpContext.User.GetAccountId(),
                Name = evnt.Name,
                Description = evnt.Description,
                Dates = evnt.Dates?.Select(d => new EventDate { Date = d.Date })
                    .ToList(),
                Crews = evnt.Crews?.Select(c => new EventCrew
                    {
                        Name = c.Name,
                        Members = c.Members.ToList()
                    })
                    .ToList(),
                Promotion = evnt.Promotion,
                PromotionType = evnt.PromotionType
            };

            try {
                this.dbContext.Add(newEvent);
                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't create new event");
                return InternalError();
            }

            this.logger.LogInformation("Created new event #{EventId} by account {AccountId}", newEvent.Id, HttpContext.User.GetAccountId());
            return Ok();
        }

        [HttpPatch("{id}"), ApiPermission("EVENT_EDIT")]
        public async Task<ActionResult> EditEvent([FromBody]EventRequest evnt, uint id)
        {
            var validationResult = validateEventRequest(evnt);
            if (validationResult != null)
                return validationResult;

            var eventDbObj = await this.dbContext.Events.Where(e => e.Id == id)
                .Include(e => e.Account)
                .Include(e => e.Dates)
                .Include(e => e.Crews)
                    .ThenInclude(e => e.Members)
                .Select(e => new
                {
                    RoleId = e.Account.RoleId,
                    AccountId = e.AccountId,
                    Dates = e.Dates,
                    Crews = e.Crews
                })
                .FirstOrDefaultAsync();

            await RetrieveRoles();
            if (eventDbObj == null || !UserCanTargetRole(eventDbObj.RoleId)
                || (evnt.AccountId != null && evnt.AccountId == HttpContext.User.GetAccountId()
                    && !UserCanTargetRole(await this.dbContext.Accounts.Where(a => a.Id == evnt.AccountId).Select(a => a.RoleId).FirstOrDefaultAsync())))
                return Forbid();

            var eventObject = new Event
            {
                Id = id
            };
            this.dbContext.Attach(eventObject);

            eventObject.Name = evnt.Name;
            eventObject.Description = evnt.Description;
            eventObject.AccountId = evnt.AccountId ?? eventDbObj.AccountId;
            eventObject.Promotion = evnt.Promotion;
            eventObject.PromotionType = evnt.PromotionType;
            eventObject.Dates = evnt.Dates?.Select(d =>
                eventDbObj.Dates.FirstOrDefault(dt => dt.Id == eventObject.Id && dt.Date.Equals(d.Date))
                ?? new EventDate
                {
                    Id = eventObject.Id,
                    Date = d.Date
                })
                .ToList();
            eventObject.Crews = evnt.Crews?.Select(c =>
                eventDbObj.Crews.Where(cr => cr.Name == c.Name)
                    .Select(cr => new EventCrew
                    {
                        Id = cr.Id,
                        CrewId = cr.CrewId,
                        Name = cr.Name,
                        Members = c.Members
                    }).FirstOrDefault()
                ??  new EventCrew
                {
                    Id = eventObject.Id,
                    Name = c.Name,
                    Members = c.Members
                })
                .ToList();

            try {
                this.dbContext.Update(eventObject);
                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't update event #{EventId}", id);
                return InternalError();
            }

            this.logger.LogInformation("Updated event #{EventId} by account {AccountId}", id, HttpContext.User.GetAccountId());
            return Ok();
        }

        [HttpDelete("{id}"), ApiPermission("EVENT_DELETE")]
        public async Task<ActionResult> DeleteEvent(uint id)
        {
            var eventObject = await this.dbContext.Events
                .Where(e => e.Id == id)
                .Include(e => e.Account)
                .Select(e => new
                {
                    RoleId = e.Account.RoleId
                })
                .FirstOrDefaultAsync();

            await RetrieveRoles();
            if (eventObject == null || !UserCanTargetRole(eventObject.RoleId))
                return Forbid();

            try {
                this.dbContext.Remove(new Event { Id = id });
                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't remove event #{EventId}", id);
                return InternalError();
            }

            this.logger.LogInformation("Removed event #{EventId} by {AccountId}", id, HttpContext.User.GetAccountId());
            return Ok();
        }

        private ActionResult validateEventRequest(EventRequest evnt)
        {
            if (evnt == null)
                return BadRequestResponse("event", BadField.Required);

            List<BadField> badFields = new List<BadField>();
            var config = this.config.GetSection("Limits:Event");
            var response = ValidationTools.ValidateString(badFields,
                evnt.Name, "name", config.GetSection("Name"), 2, 64, true);
            if (response != null)
                evnt.Name = response;

            response = ValidationTools.ValidateString(badFields,
                evnt.Description, "description", config.GetSection("Description"), 2, 65535, true);
            if (response != null)
                evnt.Description = response;

            if (evnt.Crews != null && evnt.Crews.Length > 0)
            {
                config = this.config.GetSection("Limits:EventCrew");
                var configMember = this.config.GetSection("Limits:EventCrewMember");
                foreach (var crew in evnt.Crews)
                {
                    response = ValidationTools.ValidateString(badFields,
                        crew.Name, "name", config.GetSection("Name"), 2, 32, true);
                    if (response != null)
                        crew.Name = response;

                    if (crew.Members != null && crew.Members.Count > 0)
                        foreach (var member in crew.Members)
                        {
                            response = ValidationTools.ValidateString(badFields,
                                member.Title, "title", configMember.GetSection("Title"), 2, 32, true);
                            if (response != null)
                                member.Title = response;

                            response = ValidationTools.ValidateString(badFields,
                                member.Name, "name", configMember.GetSection("Name"), 2, 65, true);
                            if (response != null)
                                member.Name = response;
                        }
                }
            }

            if (badFields.Count > 0)
                return BadRequestResponse(badFields.ToArray());

            return null;
        }
    }
}