using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Wheezie.ApiTools.Business.Authorization;
using Wheezie.ApiTools.Business.Response;
using Wheezie.ApiTools.Business.Extensions;
using Wheezie.ApiTools.Domain;
using Wheezie.ApiTools.Domain.Extensions;
using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.EF.Models;

namespace Wheezie.ApiTools.Controllers
{
    [ApiController, Route("role"), Authorize]
    public class RoleController : ApiToolsController
    {
        public RoleController(ILogger<RoleController> logger, AppDbContext dbContext, IConfiguration config)
            : base(logger, dbContext, config.GetSection("ApiTools")) { }
#region Role
        [HttpGet, ApiPermission("ROLE_VIEW")]
        public async Task<ActionResult<RoleBaseResponse>> GetRoles()
        {
            await this.RetrieveRoles();
            var roles = this.roles.Where(r => !r.Disabled && (r.Id == HttpContext.User.GetRoleId() || UserCanTargetRole(r.Id)))
                .Select(r => new RoleBaseResponse
                {
                    Id = r.Id,
                    CanTargetId = r.CanTargetId,
                    Name = r.Name,
                    Description = r.Description
                })
                .ToArray();

            if (roles == null || roles.Length <= 0)
                return NoContent();

            return Ok(roles);
        }

        [HttpGet("{id}"), ApiPermission("ROLE_VIEW")]
        public async Task<ActionResult> GetRole(uint id)
        {
            await this.RetrieveRoles(true);
            var role = await this.dbContext.Roles.Where(r => r.Id == id && !r.Disabled)
                .Include(r => r.Permissions)
                .FirstOrDefaultAsync();
            if (role == null)
                return NoContent();

            if (!UserCanTargetRole(role.Id))
                role.Permissions = null;

            return Ok(role);
        }
        #region Role Manipulation
        [HttpPost, ApiPermission("ROLE_ADD")]
        public async Task<ActionResult> AddRole(Role role)
        {
            await RetrieveRoles(true);
            var validateResult = ValidateRoleRequest(role, false);
            if (!(validateResult is OkResult))
                return validateResult;

            var badFields = ValidateRole(role);
            if (badFields != null)
                return BadRequestResponse(badFields);

            try
            {
                this.dbContext.Add(role);
                await this.dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                this.logger.LogError(ex, "An error occurred adding new role {RoleName}", role.Name);
                InternalError();
            }

            return Ok(role);
        }

        [HttpPatch, ApiPermission("ROLE_UPDATE")]
        public async Task<ActionResult> UpdateRole(Role role)
        {
            await RetrieveRoles(false);
            var validateResult = ValidateRoleRequest(role, false);
            if (!(validateResult is OkResult))
                return validateResult;

            var badFields = ValidateRole(role);
            if (badFields != null)
                return BadRequestResponse(badFields);

            try
            {
                this.dbContext.Attach(role);
                this.dbContext.Update(role);
                await this.dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException) {}

            return Ok();
        }

        [HttpDelete, ApiPermission("ROLE_DELETE")]
        public async Task<ActionResult> DeleteRole(Role role)
        {
            await RetrieveRoles(false);
            var validateResult = ValidateRoleRequest(role, false);
            if (!(validateResult is OkResult))
                return validateResult;

            var badFields = ValidateRole(role);
            if (badFields != null)
                return BadRequestResponse(badFields);

            try
            {
                this.dbContext.Remove(role);
                await this.dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException) {}

            return Ok();
        }
        #endregion
        #endregion
        #region Permission
        [HttpPut("permission"), ApiPermission("ROLE_PERM_ADD")]
        public async Task<ActionResult> AddPermission(RolePermission permission)
        {
            var validateResult = await ValidatePermissionRequest(permission);
            if (!(validateResult is OkResult))
                return validateResult;

            this.dbContext.RolePermissions.Add(permission);
            try
            {
                await this.dbContext.SaveChangesAsync();
            }
            catch(DbUpdateException)
            {
                return BadRequestResponse("permission", BadField.AlreadyExists);
            }

            logger.LogInformation("{Account} Added permission {Permission} to role {Role} with targetable role {CanTargetRole}",
                HttpContext.User.GetAccountId(),
                permission.Permission,
                permission.RoleId,
                permission.CanTargetId);
            return Ok();
        }

        [HttpPatch("permission"), ApiPermission("ROLE_PERM_UPDATE")]
        public async Task<ActionResult> UpdatePermission(RolePermission permission)
        {
            var validateResult = await ValidatePermissionRequest(permission);
            if (!(validateResult is OkResult))
                return validateResult;

            try
            {
                this.dbContext.RolePermissions.Attach(permission);
                this.dbContext.Entry(permission).Property(p => p.CanTargetId).IsModified = true;
                await this.dbContext.SaveChangesAsync();
            }
            catch(DbUpdateException)
            {
                return BadRequestResponse("permission", BadField.NotFound);
            }

            logger.LogInformation("{Account} Updated role {Role} : {Permission} targetable role to {CanTargetRole}",
                HttpContext.User.GetAccountId(),
                permission.RoleId,
                permission.Permission,
                permission.CanTargetId);
            return Ok();
        }

        [HttpDelete("permission"), ApiPermission("ROLE_PERM_REMOVE")]
        public async Task<ActionResult> RemovePermission(RolePermission permission)
        {
            var validateResult = await ValidatePermissionRequest(permission);
            if (!(validateResult is OkResult))
                return validateResult;

            try
            {
                this.dbContext.RolePermissions.Attach(permission);
                this.dbContext.RolePermissions.Remove(permission);
                await this.dbContext.SaveChangesAsync();
            }
            catch(DbUpdateException) { }

            logger.LogInformation("{Account} Removed permission {Permission} from {Role}",
                HttpContext.User.GetAccountId(),
                permission.Permission,
                permission.RoleId);
            return Ok();
        }
#endregion
        /// <summary>
        /// Validate requested permissions against the context
        /// </summary>
        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ActionResult> ValidatePermissionRequest(RolePermission permission, bool checkCanTarget = false)
        {
            await RetrieveRoles();
            if (!roles.Any(r => r.Id == permission.RoleId))
                return BadRequestResponse("roleId", BadField.Invalid);

            if (checkCanTarget && !roles.Any(r => r.Id == permission.CanTargetId))
                return BadRequestResponse("canTargetId", BadField.Invalid);

            if (!UserCanTargetRole(permission.RoleId)
                || (checkCanTarget && permission.CanTargetId.HasValue && !UserCanTargetRole(permission.CanTargetId.Value)))
                return Unauthorized();

            permission.Role = null;
            permission.CanTarget = null;

            return Ok();
        }
        /// <summary>
        /// Validate the role credentials
        /// </summary>
        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public BadField[] ValidateRole(Role role)
        {
            List<BadField> badFields = new List<BadField>();
            var response = ValidationTools.ValidateString(role.Name, "name", true,
                config.GetLimitedInt("Limits:Role:Name:Minimum", 1, 32),
                config.GetLimitedInt("Limits:Role:Name:Maximum", 1, 32));
            if (!response.Valid)
                badFields.Add(response.BadField);
            else
                role.Name = (string)response.ModifiedValue;

            response = ValidationTools.ValidateString(role.Description, "description",
                config.GetValue<bool>("Limits:Role:Description:Required"),
                config.GetLimitedInt("Limits:Role:Description:Minimum", 1, 512),
                config.GetLimitedInt("Limits:Role:Description:Maximum", 1, 512));
            if (!response.Valid)
                badFields.Add(response.BadField);
            else
                role.Name = (string)response.ModifiedValue;


            if (badFields.Count == 0)
                return null;
            return badFields.ToArray();
        }
        /// <summary>
        /// Validate requested role against the context
        /// </summary>
        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ActionResult ValidateRoleRequest(Role role, bool checkRoleId = true)
        {
            List<BadField> badFields = new List<BadField>();
            if (checkRoleId && !roles.Any(r => r.Id == role.Id))
                badFields.Add(new BadField("id", BadField.Invalid));

            if (role.CanTargetId != null && !roles.Any(r => r.Id == role.CanTargetId))
                badFields.Add(new BadField("canTargetId", BadField.Invalid));

            if ((checkRoleId && !UserCanTargetRole(role.Id))
                || (role.CanTargetId.HasValue && !UserCanTargetRole(role.CanTargetId.Value)))
                return Forbid();

            if (badFields.Count > 0)
                return BadRequestResponse(badFields.ToArray());

            role.CanTarget = null;

            return Ok();
        }
    }
}