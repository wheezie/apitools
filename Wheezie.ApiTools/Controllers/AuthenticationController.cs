using System.Security.Claims;
using System.Net;
using System.Globalization;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Wangkanai.Detection.Services;

using Wheezie.ApiTools.Business.Authentication;
using Wheezie.ApiTools.Business.Authorization;
using Wheezie.ApiTools.Business.Extensions;
using Wheezie.ApiTools.Business.Request;
using Wheezie.ApiTools.Business.Response;
using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.EF.Models;
using Wheezie.ApiTools.Domain;

namespace Wheezie.ApiTools.Controllers
{
    [ApiController, Route("auth"), ApiPermission]
    public class AuthenticationController : ApiToolsController
    {
        private readonly PasswordHasher passwordHasher;
        private readonly IDetectionService detection;

        public AuthenticationController(ILogger<AuthenticationController> logger, AppDbContext dbContext, IConfiguration config,
            PasswordHasher passwordHasher, IDetectionService detection)
            : base(logger, dbContext, config.GetSection("ApiTools"))
        {
            this.passwordHasher = passwordHasher;
            this.detection = detection;
        }

        /// <summary>
        /// Setup the administrator account's password
        /// </summary>
        /// <param name="reqAccount">Account details</param>
        /// <returns></returns>
        /// <response code="200">Account retrieval succeeded</response>
        /// <response code="400">Invalid setup password provided</response>
        /// <response code="405">Setup already completed</response>
        [HttpPost("setup"), AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        public async Task<ActionResult> Setup(AuthBaseRequest reqAccount)
        {
            if (!config.GetValue<bool>("Security:Setup", false)
                || !await this.dbContext.Accounts.AnyAsync(a => a.Id == 1
                    && (a.State == AccountState.Confirmed || !string.IsNullOrWhiteSpace(a.Password))))
                return StatusCode(405);

            var result = AccountController.VerifyPassword(reqAccount.Password, config);

            if (!result.Valid)
                return BadRequestResponse(result.BadField);

            var account = new Account { Id = 1 };
            this.dbContext.Attach(account);
            account.Password = this.passwordHasher.GenerateHash(result.ModifiedValue);
            account.State = AccountState.Confirmed;
            await this.dbContext.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Verifies a current session and retrieves the latest account details.
        /// </summary>
        /// <returns>The latest account details and permissions</returns>
        /// <response code="200">Account retrieval succeeded</response>
        /// <response code="401">Authentication required</response>
        [HttpGet("verify")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<AccountRoleResponse>> Verify()
        {
            var details = await dbContext.Accounts.Where(a => a.Id == AccountId)
                    .AsNoTracking()
                    .Include(a => a.Role)
                        .ThenInclude(a => a.Permissions)
                    .Include(a => a.Emails)
                    .Select(a => new AccountRoleResponse(a, config))
                    .FirstOrDefaultAsync();

            return Ok(details);
        }

        /// <summary>
        /// Authenticates and retrieves the latest identity.
        /// </summary>
        /// <returns>The latest account details and permissions</returns>
        /// <response code="200">Authentication succeded</response>
        /// <response code="400">Invalid credentials or account is not confirmed.</response>
        /// <response code="401">Authentication failed (incorrect credentials or account has been disabled)</response>
        [HttpPost("login"), AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IdentityResponse>> Authenticate(AuthRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Username))
                return BadRequestResponse("username", BadField.Required);
            else if (string.IsNullOrWhiteSpace(request.Password))
                return BadRequestResponse("password", BadField.Invalid);

            var account = await this.dbContext.Accounts
                .Include(a => a.Role)
                    .ThenInclude(r => r.Permissions)
                .Include(a => a.Emails)
                .Where(a => (a.Username.ToLower() == request.Username.ToLower()
                        || a.Emails.FirstOrDefault(e => e.Type == EmailType.Primary).Email.ToLower() == request.Username.ToLower()))
                .Select(a => new
                {
                    a.Id,
                    a.Password,
                    a.State,
                    Identity = new AccountRoleResponse(a, config)
                })
                .FirstOrDefaultAsync();
            switch (account?.State)
            {
                case AccountState.Unconfirmed:
                    return BadRequestResponse("account", BadField.NotConfirmed);
                case AccountState.Confirmed:
                    if (!this.passwordHasher.Verify(request.Password, account.Password))
                        return Unauthorized();
                    break;
                default:
                    return Unauthorized();
            }

            var session = await generateSession(account.Id);
            if (session == null)
                return InternalError();

            return Ok(new IdentityResponse(session.Id, account.Identity));
        }

        /// <summary>
        /// Sign the current session out
        /// </summary>
        /// <returns>The latest account details and permissions</returns>
        /// <response code="200">Signout succeeded</response>
        /// <response code="401">Authentication required</response>
        [HttpPost("signout")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> Signout()
        {
            Guid session = HttpContext.User.GetSessionToken();
            await this.dbContext.Sessions.Where(s => s.Id == session)
                .ForEachAsync(s => s.Expires = DateTime.UtcNow);

            await this.dbContext.SaveChangesAsync();
            this.logger.LogInformation("Logged out session with token: {Token} used by ", session);

            return Ok();
        }

        /// <summary>
        /// Change your password
        /// </summary>
        /// <param name="request">New and old password for changing</param>
        /// <returns></returns>
        /// <response code="200">Change succeeded</response>
        /// <response code="401">Authentication required</response>
        /// <response code="400">Invalid request contents</response>
        [HttpPost("changePassword")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> ChangePassword(AuthRequest request)
        {
            var account = await this.dbContext.Accounts
                .Where(a => a.Id == AccountId)
                .Select(a => new Account
                {
                    Id = a.Id,
                    Password = a.Password
                })
                .FirstAsync();

            var result = AccountController.VerifyPassword(request.NewPassword, config);
            if (!result.Valid)
                return BadRequestResponse("newPassword", BadField.Invalid);

            if (!passwordHasher.Verify(request.Password, account.Password))
                return BadRequestResponse("password", BadField.Invalid);


            account.Password = passwordHasher.GenerateHash(result.ModifiedValue);
            this.dbContext.Update(account);
            await this.dbContext.SaveChangesAsync();
            this.logger.LogInformation("Changed password for {AccountId} with ip: {IPAddress}", account.Id, HttpContext.Connection.RemoteIpAddress);

            return Ok();
        }

        /// <summary>
        /// Sign out all sessions for this account
        /// </summary>
        /// <param name="generateOne">Should generate a new session</param>
        /// <returns>A tokenresponse containing a new session token (if requested)</returns>
        /// <response code="200">Signout succeeded</response>
        /// <response code="401">Authentication required</response>
        /// <response code="500">Couldn't create a new session or sign out all.</response>
        [HttpPost]
        [Route("signoutEverywhere")]
        [Route("signoutEverywhere/{generateOne}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<TokenResponse>> SignoutEverywhere(bool generateOne = false)
        {
            var accountId = AccountId;
            await this.dbContext.Sessions.Where(s => s.AccountId == accountId)
                .ForEachAsync(s => s.Expires = DateTime.UtcNow);

            if (generateOne)
            {
                var session = await generateSession(accountId);
                if (session != null)
                    return Ok(new TokenResponse
                    {
                        Token = session.Id.ToString("N")
                    });
                else
                    return InternalError();
            }
            await this.dbContext.SaveChangesAsync();
            this.logger.LogInformation("Signed out all sessions for {AccountId}", accountId);

            return Ok();
        }

        private async Task<Session> generateSession(ulong accountId)
        {
            var session = new Session
            {
                AccountId = accountId,
                Browser = (detection.Browser != null) ?
                    (Browser)Convert.ToByte(detection.Browser.Name) : Browser.Unknown,
                Platform = (detection.Platform != null) ?
                    (Platform)Convert.ToByte(detection.Platform.Name) : Platform.Other,
                Ip = HttpContext.Connection.RemoteIpAddress.ToString(),
                Issued = DateTime.UtcNow,
                Expires = DateTime.UtcNow.AddSeconds(this.config.GetValue<uint>("Security:Session:Lifetime"))
            };

            try {
                this.dbContext.Sessions.Add(session);
                await this.dbContext.SaveChangesAsync();
            } catch(Exception ex) {
                this.logger.LogError(ex, "An error occurred creating a session for {AccountId}", accountId);
                return null;
            }

            this.logger.LogInformation("Generated session for {AccountId}: {Session}, IP: {IPAddress}", accountId, session.Id, session.Ip);
            return session;
        }
    }
}