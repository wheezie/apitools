using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Wheezie.ApiTools.Business.Authentication;
using Wheezie.ApiTools.Business.Authorization;
using Wheezie.ApiTools.Business.Extensions;
using Wheezie.ApiTools.Business.Pictures;
using Wheezie.ApiTools.Business.Response;
using Wheezie.ApiTools.Business.Request;
using Wheezie.ApiTools.Domain;
using Wheezie.ApiTools.Domain.Extensions;
using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.EF.Models;

namespace Wheezie.ApiTools.Controllers
{
    [ApiController, Route("account"), Authorize]
    public class AccountController : ApiToolsController
    {
        private readonly PasswordHasher passwordHasher;

        public AccountController(ILogger<AccountController> logger, AppDbContext dbContext, IConfiguration config, PasswordHasher passwordHasher)
            : base(logger, dbContext, config.GetSection("ApiTools"))
        {
            this.passwordHasher = passwordHasher;
        }

        /// <summary>
        /// Retrieve own account details.
        /// </summary>
        /// <returns>Authenticated user's account</returns>
        /// <response code="200">Account retrieval succeeded</response>
        /// <response code="401">Authentication required</response>
        /// <response code="404">The account is not found</response>
        [HttpGet, ApiPermission("ACCOUNT_VIEW")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AccountResponse>> GetAccount()
        {
            var account = await this.dbContext.Accounts.Where(a => a.Id == HttpContext.User.GetAccountId())
                .Include(a => a.ProfileEmail)
                .Select(a => new AccountResponse(a, config))
                .FirstOrDefaultAsync();

            if (account == null)
                return NotFound();

            return Ok(account);
        }

        /// <summary>
        /// Change an account's state
        /// </summary>
        /// <param name="request">AccountRequest containing the account id and the state to change to.</param>
        /// <returns>Succeeded state</returns>
        /// <response code="200">Account update succeeded</response>
        /// <response code="400">Request body doesn't contain an Id and state</response>
        /// <response code="401">Authorization required</response>
        /// <response code="403">Access denied</response>
        /// <response code="500">Internal error prevented update</response>
        [HttpPost("state"), ApiPermission("ACCOUNT_STATE")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> ChangeState(AccountRequest request)
        {
            await RetrieveRoles(false);
            var account = await this.dbContext.Accounts
                .FirstOrDefaultAsync(a => a.Id == request.Id);

            if (account == null || !UserCanTargetRole(account.RoleId))
                return Forbid();

            bool confirmed = (byte)account.State >= 100 && (byte)account.State < 200;
            switch (request.State)
            {
                case AccountState.Banned:
                case AccountState.ConfirmedBanned:
                    account.State = confirmed ? AccountState.ConfirmedBanned : AccountState.Banned;
                    break;
                case AccountState.Blocked:
                case AccountState.ConfirmedBlocked:
                    account.State = confirmed ? AccountState.ConfirmedBlocked : AccountState.Blocked;
                    break;
                case AccountState.Confirmed:
                case AccountState.Unconfirmed:
                    account.State = request.State;
                    break;
                default:
                    return BadRequestResponse("state", BadField.Invalid);
            }

            try
            {
                await this.dbContext.SaveChangesAsync();
            }
            catch(DbUpdateException ex)
            {
                this.logger.LogError(ex, "Couldn't update account state for {Account} to {State}", account.Id, account.State);
                return InternalError();
            }

            return Ok();
        }

        /// <summary>
        /// Create an invite token
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Account update succeeded</response>
        /// <response code="401">Authentication required</response>
        /// <response code="405">Inviting is not enabled on this API.</response>
        /// <response code="500">Internal error prevented creation</response>
        [HttpPost("invite"), ApiPermission("ACCOUNT_INVITE")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<TokenResponse>> CreateInvite()
        {
            if (!config.GetValue<bool>("Security:Invite:Enabled"))
                return StatusCode(405);

            var invite = new AccountInvite
            {
                InviterId = HttpContext.User.GetAccountId(),
                Invited = DateTime.UtcNow,
                Expire = DateTime.UtcNow.AddMinutes(config.GetValue<uint>("Security:Invite:Lifetime")),
            };

            try
            {
                this.dbContext.AccountInvite.Add(invite);
                await this.dbContext.SaveChangesAsync();
            }
            catch(DbUpdateException ex)
            {
                this.logger.LogError(ex, "Couldn't create invite token");
                return InternalError();
            }

            this.logger.LogInformation("Account #{Account} created an invite token: {Token}", invite.InviterId, invite.Token);
            return Ok(new TokenResponse { Token = invite.Token.ToString("N") });
        }

        /// <summary>
        /// Register an account.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Succeeded state</returns>
        /// <response code="200">Registration completed successfully</response>
        /// <response code="400">Some request details were invalid</response>
        /// <response code="405">Registration is not enabled</response>
        /// <response code="500">Couldn't register account.</response>
        [HttpPost("register"), AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> Register(RegistrationRequest request)
        {
            if (!config.GetValue<bool>("Security:Registration:Enabled"))
                return StatusCode(405);

            var result = await handleRegistration(request);
            if (result.Result != null)
                return result.Result;

            this.logger.LogInformation("New account {Account} was created: {Username} by {Ip}",
                result.Value.Id,
                result.Value.Username,
                HttpContext.Connection.RemoteIpAddress.ToString());
            return Ok();
        }

        /// <summary>
        /// Register an account using an invite token.
        /// </summary>
        /// <param name="request">Registration account details</param>
        /// <param name="inviteToken">Invite token</param>
        /// <returns>Register an invited account</returns>
        /// <response code="200">Registration completed successfully</response>
        /// <response code="400">Some request (or token) details where invalid</response>
        /// <response code="405">Registration with invite not enabled</response>
        /// <response code="500">Couldn't register account</response>
        [HttpPost("register/{inviteToken}"), AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> RegisterInvited(RegistrationRequest request, Guid inviteToken)
        {
            if (!config.GetValue<bool>("Security:Invite:Enabled"))
                return StatusCode(405);

            var invite = await this.dbContext.AccountInvite.FirstOrDefaultAsync(i => i.Token == inviteToken
                && i.InviterId != null && i.AcceptorId == null && i.Expire > DateTime.UtcNow);
            if (invite == null)
                return BadRequestResponse("token", BadField.Invalid);

            using (var transaction = this.dbContext.Database.BeginTransaction())
            {
                var result = await handleRegistration(request);
                if (result.Result != null)
                    return result.Result;

                try
                {
                    invite.Acceptor = result.Value;
                    await this.dbContext.SaveChangesAsync();
                }
                catch(DbUpdateException ex)
                {
                    this.logger.LogError(ex, "An error occurred saving new account by invited token {Token}", inviteToken.ToString("N"));
                    await transaction.RollbackAsync().ConfigureAwait(false);
                    return InternalError();
                }

                this.logger.LogInformation("Created new account #{AccountId} with username: {Username} using token: {Token} using ip: {IpAddress}",
                    result.Value.Id,
                    result.Value.Username,
                    inviteToken.ToString("N"),
                    HttpContext.Connection.RemoteIpAddress.ToString());
                await transaction.CommitAsync(HttpContext.RequestAborted).ConfigureAwait(false);
            }
            return Ok();
        }

        /// <summary>
        /// Upload a new profile picture
        /// </summary>
        /// <param name="file">Formfile containing the picture to process</param>
        /// <returns>The succeeded picture</returns>
        /// <response code="200">Profile picture updated.</response>
        /// <response code="400">Provided picture is invalid.</response>
        /// <response code="401">Authentication required.</response>
        /// <response code="500">Couldn't complete picture update.</response>
        [HttpPost("image"), ApiPermission("ACCOUNT_IMAGE"), RequestSizeLimit(1*1024*1024)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ProfilePictureResponse>> UploadProfilePicture(IFormFile file)
        {
            if (file == null || file.Length <= 0)
                return BadRequestResponse("file", BadField.Invalid);

            using var transaction = this.dbContext.Database.BeginTransaction();
            try {
                var account = await this.dbContext.Accounts.Where(a => a.Id == HttpContext.User.GetAccountId())
                    .Select(a => new Account
                    {
                        Id = a.Id,
                        PictureId = a.PictureId
                    })
                    .FirstOrDefaultAsync();
                Guid? oldPicture = account.PictureId;

                var newPicture = await UploadPicture(file, PictureFormat.PROFILE_PICTURES, transaction);
                if (newPicture == null) {
                    return InternalError();
                }

                account.PictureId = newPicture.Id;
                this.dbContext.Entry(account).Property(a => a.PictureId).IsModified = true;
                await this.dbContext.SaveChangesAsync();

                if (oldPicture.HasValue)
                    await RemovePicture(oldPicture.Value);

                await transaction.CommitAsync();
                this.logger.LogInformation("Updated profile picture for #{AccountId} to #{PictureId}", HttpContext.User.GetAccountId(), account.PictureId);
                return Ok(new ProfilePictureResponse
                {
                    Id = newPicture.Id,
                    Picture = newPicture.Id.AsProfilePicture(config),
                    Uploaded = newPicture.Uploaded
                });
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't replace profile picture for {AccountId}", HttpContext.User.GetAccountId());
                await transaction.RollbackAsync();
                return InternalError();
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ActionResult<Account>> handleRegistration(RegistrationRequest request)
        {
            List<BadField> badFields = new List<BadField>();
            var limitConfig = this.config.GetSection("Limits:Account");
            Account account = new Account
            {
                State = AccountState.Unconfirmed,
                RegisteredDate = DateTime.UtcNow
            };

            var result = ValidationTools.ValidateString(request.Username, "username", true,
                limitConfig.GetLimitedInt("Username:Minimum", 2, 32, 2),
                limitConfig.GetLimitedInt("Username:Maximum", 2, 32, 32));
            if (result.Valid)
                account.Username = result.ModifiedValue;
            else
                badFields.Add(result.BadField);

            bool emailRequired = limitConfig.GetValue<bool>("Email:Required", true);
            result = ValidationTools.ValidateEmailString(request.Email, "email");
            if (result.Valid)
            {
                var accountEmail = await this.dbContext.AccountEmails.FirstOrDefaultAsync(e => e.Email == (string)result.ModifiedValue);
                if (accountEmail == null)
                    account.Emails.Add(new AccountEmail
                    {
                        Email = result.ModifiedValue,
                        Type = EmailType.Primary,
                        Verified = false
                    });
                else if (accountEmail.AccountId != null || accountEmail.Type == EmailType.Banned)
                    badFields.Add(new BadField("email", BadField.AlreadyExists));
                else
                {
                    accountEmail.Type = EmailType.Primary;
                    accountEmail.Verified = false;
                    account.Emails.Add(accountEmail);
                }
            }
            else if (emailRequired)
                badFields.Add(result.BadField);

            result = ValidationTools.ValidateString(request.FirstName, "firstName",
                limitConfig.GetValue<bool>("FirstName:Required"),
                limitConfig.GetLimitedInt("FirstName:Minimum", 2, 32, 2),
                limitConfig.GetLimitedInt("FirstName:Maximum", 2, 32, 32));
            if (result.Valid)
                account.FirstName = result.ModifiedValue;
            else
                badFields.Add(result.BadField);

            result = ValidationTools.ValidateString(request.LastName, "lastName",
                limitConfig.GetValue<bool>("LastName:Required"),
                limitConfig.GetLimitedInt("LastName:Minimum", 2, 32, 2),
                limitConfig.GetLimitedInt("LastName:Maximum", 2, 32, 32));
            if (result.Valid)
                account.LastName = result.ModifiedValue;
            else
                badFields.Add(result.BadField);

            result = VerifyPassword(request.Password, config);
            if (result.Valid)
                account.Password = passwordHasher.GenerateHash(result.ModifiedValue);
            else
                badFields.Add(result.BadField);

            if (badFields.Count > 0)
                return BadRequestResponse(badFields.ToArray());

            try
            {
                this.dbContext.Add(account);
                await this.dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                this.logger.LogError(ex, "An error occurred adding new account.");
                return InternalError();
            }

            return account;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public static ValidationResponse VerifyPassword(string password, IConfigurationSection config)
        {
            var limitConfig = config.GetSection("Limits:Account:Password");
            var result = ValidationTools.ValidatePasswordString(password, "password",
                Math.Max(limitConfig.GetValue("Minimum", 6), 6),
                limitConfig.GetValue("Number", true),
                limitConfig.GetValue("SpecialCharacter", true));

            return result;
        }
    }
}