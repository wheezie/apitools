using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Wheezie.ApiTools.Business.Authorization;
using Wheezie.ApiTools.Business.Extensions;
using Wheezie.ApiTools.Business.Pictures;
using Wheezie.ApiTools.Business.Response;
using Wheezie.ApiTools.Domain;
using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.EF.Models;

namespace Wheezie.ApiTools.Controllers
{
    [ApiController, Route("album"), Authorize]
    public class AlbumController : ApiToolsController
    {
        public AlbumController(ILogger<AlbumController> logger, AppDbContext dbContext, IConfiguration config)
            : base(logger, dbContext, config.GetSection("ApiTools")) { }

        /// <summary>
        /// Retrieve a list of visibile albums
        /// </summary>
        /// <returns>A list of albums formatted for public use</returns>
        /// <response code="200">List of albums</response>
        /// <response code="204">No albums found</response>
        [HttpGet, ApiPermission("ALBUM_VIEW", false), ResponseCache(Duration = 300, Location = ResponseCacheLocation.Any)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<AlbumResponse[]>> GetAlbums()
        {
            var albums = await getAlbumQueryable()
                .Select(a => new AlbumResponse
                {
                    Id = a.Id,
                    Description = a.Description,
                    Account = new AccountResponse(a.Account, config),
                    Likes = a.Likes.LongCount(l => !l.Unliked)
                })
                .ToArrayAsync();

            if (albums.Length <= 0)
                return NoContent();

            return Ok(albums);
        }

        /// <summary>
        /// Create a new album
        /// </summary>
        /// <param name="album">Album request content</param>
        /// <returns>The newly created album (basic response)</returns>
        /// <response code="200">Newly created album</response>
        /// <response code="400">Album request content invalid</response>
        /// <response code="403">Authentication required</response>
        /// <response code="500">Couldn't create a new album</response>
        [HttpPost, ApiPermission("ALBUM_NEW")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<AlbumResponse>> NewAlbum([FromBody]Album album)
        {
            List<BadField> badFields = new List<BadField>();
            var config = this.config.GetSection("Limits:Blog");
            var response = ValidationTools.ValidateString(badFields,
                album.Name, "name", config.GetSection("Name"), 2, 48);
            if (response != null)
                album.Name = response;

            response = ValidationTools.ValidateString(badFields,
                album.Description, "description", config.GetSection("Description"), 0, 255, false);
            if (response != null)
                album.Description = response;

            if (badFields.Count > 0)
                return BadRequestResponse(badFields.ToArray());

            album.Pictures.Clear();
            album.Likes.Clear();
            album.AccountId = HttpContext.User.GetAccountId();

            try {
                this.dbContext.Add(album);
                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't create new album by {Account}", album.AccountId);
                return InternalError();
            }

            return Ok(new AlbumResponse
            {
                Id = album.Id,
                Description = album.Description
            });
        }

        /// <summary>
        /// Retrieve an visible album
        /// </summary>
        /// <param name="albumId">Album identifier</param>
        /// <returns>Album objects</returns>
        /// <response code="200">Retrieves album successfully</response>
        /// <response code="404">Album is not accessable/found.</response>
        [HttpGet("{albumId}"), ApiPermission("ALBUM_VIEW", false), ResponseCache(Duration = 60, Location = ResponseCacheLocation.Any)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AlbumResponse>> GetAlbum(uint albumId)
        {
            var album = await getAlbumQueryable().Where(a => a.Id == albumId)
                .Include(a => a.Pictures)
                    .ThenInclude(p => p.Picture)
                .Select(a => new AlbumResponse
                {
                    Id = a.Id,
                    Description = a.Description,
                    Account = new AccountResponse(a.Account, config),
                    Pictures = a.Pictures
                        .Where(p => (byte)p.Picture.State <= (byte)PictureState.Public)
                        .Select(p => new AlbumPictureResponse(p.Picture, config)),
                    Likes = a.Likes.Count(l => !l.Unliked)
                })
                .FirstOrDefaultAsync();
            if (album == null)
                return NotFound();

            return Ok(album);
        }

        /// <summary>
        /// Delete an album by it's identifier
        /// </summary>
        /// <param name="albumId">Album identifier</param>
        /// <returns></returns>
        /// <response code="200">Album deleted successfully</response>
        /// <response code="401">Authentication required</response>
        /// <response code="403">Access denied</response>
        /// <response code="500">Couldn't delete the album</response>
        [HttpDelete("{albumId}"), ApiPermission("ALBUM_DELETE")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteAlbum(uint albumId)
        {
            var album = await this.dbContext.Albums.Include(a => a.Account)
                .Where(a => a.Id == albumId)
                .Include(a => a.Account)
                .Include(a => a.Pictures)
                .Select(a => new Album
                {
                    Account = new Account
                    {
                        Id = a.Account.Id,
                        RoleId = a.Account.RoleId
                    },
                    Pictures = a.Pictures
                })
                .AsNoTracking()
                .FirstOrDefaultAsync();
            await RetrieveRoles();
            if (album == null || !UserCanTargetRole(album.Account))
                return Forbid();

            using (var transaction = await this.dbContext.Database.BeginTransactionAsync())
            {
                try {
                    List<Task> tasks = new List<Task>();
                    foreach (var albumPicture in album.Pictures)
                        tasks.Add(RemovePicture(albumPicture.PictureId, true));
                    this.dbContext.AlbumPictures.RemoveRange(album.Pictures);
                    this.dbContext.AlbumLikes.RemoveRange(this.dbContext.AlbumLikes.Where(p => p.Id == albumId));

                    tasks.Add(this.dbContext.SaveChangesAsync());
                    Task.WaitAll(tasks.ToArray());

                    this.dbContext.Albums.Remove(new Album { Id = albumId });
                    await this.dbContext.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return Ok();
                } catch (DbUpdateConcurrencyException ex) {
                    this.logger.LogError(ex, "Couldn't remove album {albumId}");
                    await transaction.RollbackAsync();
                    return InternalError();
                }
            }
        }

        /// <summary>
        /// Toggle like/unlike an album
        /// </summary>
        /// <param name="albumId">Album identifier</param>
        /// <returns></returns>
        /// <response code="200">Album liked/unliked</response>
        /// <response code="401">Authentication required</response>
        /// <response code="404">Album not found</response>
        /// <response code="500">Couldn't delete the album</response>
        [HttpPost("{albumId}/like"), ApiPermission("ALBUM_LIKE")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> LikeAlbum(uint albumId)
        {
            ulong accountId = HttpContext.User.GetAccountId();
            if (!await this.dbContext.Albums.AnyAsync(a => a.Id == albumId))
                return NotFound();

            try {
                var albumLike = await this.dbContext.AlbumLikes.FirstOrDefaultAsync(l => l.LikerId == accountId && l.Id == albumId);
                if (albumLike == null) {
                    this.dbContext.AlbumLikes.Add(new AlbumLike
                    {
                        Id = albumId,
                        LikerId = accountId,
                        Date = DateTime.UtcNow
                    });
                } else if (albumLike.Unliked && albumLike.Date.AddSeconds(3) <= DateTime.UtcNow) {
                    albumLike.Unliked = false;
                    albumLike.Date = DateTime.UtcNow;
                    this.dbContext.AlbumLikes.Update(albumLike);
                } else
                    return Ok();
                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "An error occurred liking album {Album} by {Account}", albumId, accountId);
                return InternalError();
            }

            this.logger.LogInformation("Account {Account} liked album {Album}", accountId, albumId);
            return Ok();
        }

        /// <summary>
        /// Unlike an album
        /// </summary>
        /// <param name="albumId">Album identifier</param>
        /// <returns></returns>
        /// <response code="200">Album unliked</response>
        /// <response code="401">Authentication required</response>
        /// <response code="404">Album not found</response>
        /// <response code="500">Couldn't delete the album</response>
        [HttpDelete("{albumId}/like"), ApiPermission("ALBUM_LIKE")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> UnlikeAlbum(uint albumId)
        {
            ulong accountId = HttpContext.User.GetAccountId();
            if (!await this.dbContext.Albums.AnyAsync(a => a.Id == albumId && a.Likes.Any(l => l.LikerId == accountId)))
                return NotFound();

            try {
                var like = new AlbumLike
                {
                    Id = albumId,
                    LikerId = accountId
                };
                this.dbContext.Attach(like);
                like.Unliked = true;
                like.Date = DateTime.UtcNow;

                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "An error occurred adding removing like for album {Album} by {Account}", albumId, accountId);
                return InternalError();
            }

            this.logger.LogInformation("Account {Account} unliked album {Album}", accountId, albumId);
            return Ok();
        }

        /// <summary>
        /// Upload a picture to an album
        /// </summary>
        /// <param name="albumId">Album identifier</param>
        /// <param name="file">Picture to upload</param>
        /// <returns>Picture object</returns>
        /// <response code="200">Picture uploaded sucessfully</response>
        /// <response code="400">Picture invalid</response>
        /// <response code="401">Authentication required</response>
        /// <response code="403">Access denied</response>
        /// <response code="404">Album not found</response>
        /// <response code="500">Couldn't upload the picture</response>
        [HttpPut("{albumId}/image"), ApiPermission("ALBUM_PICTURE_UPLOAD"), RequestSizeLimit(20*1024*1024)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<AlbumPictureResponse>> UploadImage(uint albumId, IFormFile file)
        {
            var album = await this.dbContext.Albums.Where(a => a.Id == albumId)
                .Include(a => a.Account)
                .Select(a => new Album
                {
                    Id = albumId,
                    AccountId = a.AccountId,
                    Account = new Account
                    {
                        Id = a.Account.Id,
                        RoleId = a.Account.RoleId
                    }
                })
                .FirstOrDefaultAsync();
            if (album == null)
                return NotFound();

            await RetrieveRoles();
            if (!UserCanTargetRole(album.Account))
                return Forbid();

            if (file == null || file.Length <= 0)
                return BadRequestResponse("image", BadField.Invalid);

            using var transaction = this.dbContext.Database.BeginTransaction();
            Picture picture;
            try {
                picture = await UploadPicture(file, PictureFormat.PICTURES, transaction);
                if (picture == null)
                    return InternalError();
            } catch (SixLabors.ImageSharp.UnknownImageFormatException) {
                await transaction.RollbackAsync();
                return BadRequestResponse("image", BadField.Invalid);
            }

            AlbumPicture albumPicture = new AlbumPicture
            {
                Id = albumId,
                Picture = picture
            };

            try {
                this.dbContext.Add(albumPicture);
                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't save picture {Picture} to album {Album}", picture.Id, albumId);
                await transaction.RollbackAsync();
                return InternalError();
            }

            await transaction.CommitAsync();
            return Ok(new AlbumPictureResponse
            {
                Id = picture.Id,
                Picture = picture.Id.AsAlbumPicture(config),
                Uploaded = picture.Uploaded
            });
        }

        /// <summary>
        /// Delete an album picture
        /// </summary>
        /// <param name="albumId">Album identifier</param>
        /// <param name="pictureId">Picture identifier</param>
        /// <returns></returns>
        /// <response code="200">Picture deleted sucessfully</response>
        /// <response code="401">Authentication required</response>
        /// <response code="403">Access denied</response>
        /// <response code="404">Album not found</response>
        /// <response code="500">Couldn't delete the picture</response>
        [HttpDelete("{albumId}/{pictureId}"), ApiPermission("ALBUM_PICTURE_DELETE")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteImage(uint albumId, Guid pictureId)
        {
            var albumPicture = await this.dbContext.AlbumPictures.Where(a => a.Id == albumId && a.PictureId == pictureId)
                .Include(a => a.Album)
                    .ThenInclude(a => a.Account)
                .Select(a => new AlbumPicture
                {
                    Id = albumId,
                    Album = new Album {
                        Account = new Account
                        {
                            Id = a.Album.Account.Id,
                            RoleId = a.Album.Account.RoleId
                        }
                    },
                    PictureId = pictureId
                })
                .AsNoTracking()
                .FirstOrDefaultAsync();
            if (albumPicture == null)
                return NotFound();

            await RetrieveRoles();
            if (!UserCanTargetRole(albumPicture.Album.Account))
                return Forbid();

            using var transaction = this.dbContext.Database.BeginTransaction();
            try {
                var removePicture = new AlbumPicture
                {
                    Id = albumId,
                    PictureId = pictureId
                };
                this.dbContext.AlbumPictures.Remove(removePicture);
                await this.dbContext.SaveChangesAsync();

                if (await RemovePicture(pictureId, false)) {
                    await transaction.CommitAsync();
                    return Ok();
                } else {
                    await transaction.RollbackAsync();
                    return InternalError();
                }
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't remove album picture #{AlbumId}.{PictureId}", albumId, pictureId);
                await transaction.RollbackAsync();
                return InternalError();
            }
        }

        private IQueryable<Album> getAlbumQueryable(bool overrideAuth = false)
        {
           IQueryable<Album> query = this.dbContext.Albums
                .Include(a => a.Account)
                .Include(a => a.Likes);
            if (!HttpContext.User.Identity.IsAuthenticated && !overrideAuth)
                return query.Where(a => a.Pictures.Count > 0);

            return query;
        }

        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public bool UserCanTargetRole(Account account)
                => (account.Id == AccountId) || base.UserCanTargetRole(account.RoleId);
    }
}