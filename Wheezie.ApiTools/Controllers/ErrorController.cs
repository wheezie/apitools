using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Wheezie.ApiTools.Controllers
{
    [ApiController, Route("error")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : ApiControllerBase
    {
        private readonly ILogger<ErrorController> logger;
        public ErrorController(ILogger<ErrorController> logger)
        {
            this.logger = logger;
        }

        [HttpGet("500")]
        public ActionResult InternalErrorOccurred()
        {
            var exception =
                HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exception == null)
                return InternalError();

            logger.LogError(exception.Error,
                "An uncaught exception occurred for request with trace {Trace}",
                HttpContext.TraceIdentifier);
            return InternalError(new
            {
                Trace = HttpContext.TraceIdentifier
            });
        }
    }
}