using System.Collections.Concurrent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Wheezie.ApiTools.Business.Authorization;
using Wheezie.ApiTools.Business.Extensions;
using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.EF.Models;
using Wheezie.ApiTools.Domain;

namespace Wheezie.ApiTools.Controllers
{
    [ApiController, Route("blog")]
    public class BlogController : ApiToolsController
    {
        public BlogController(ILogger<BlogController> logger, AppDbContext dbContext, IConfiguration config)
            : base(logger, dbContext, config.GetSection("ApiTools")) {}

        [HttpGet, ApiPermission("BLOG_VIEW", false), ResponseCache(Duration = 300, Location = ResponseCacheLocation.Any)]
        public async Task<ActionResult> GetBlogs()
        {
            var blogs = await getAuthedQueryable()
                .OrderByDescending(b => b.Created)
                .Select(b => new
                {
                    b.Id,
                    b.Name,
                    b.Description,
                    b.Created,
                    Creator = new
                    {
                        b.Creator.Username,
                        b.Creator.FirstName,
                        b.Creator.LastName,
                        Picture = b.Creator.PictureId.AsProfilePicture(config),
                        b.Creator.ShowName
                    },
                    b.Visibility
                })
                .ToArrayAsync();
            if (blogs == null || blogs.Length <= 0)
                return NoContent();

            return Ok(blogs);
        }

#region Blogs
        [HttpGet("{blogId}"), ApiPermission("BLOG_VIEW", false), ResponseCache(Duration = 60, Location = ResponseCacheLocation.Any)]
        public async Task<ActionResult> GetBlog(uint blogId)
        {
            var blog = await getAuthedQueryable()
                .Where(b => b.Id == blogId)
                .OrderByDescending(b => b.Created)
                .Include(b => b.Posts)
                    .ThenInclude(p => p.Account)
                        .ThenInclude(a => a.Picture)
                .Select(b => new
                {
                    Id = b.Id,
                    Name = b.Name,
                    Description = b.Description,
                    Created = b.Created,
                    Creator = new
                    {
                        Username = b.Creator.Username,
                        FirstName = b.Creator.FirstName,
                        LastName = b.Creator.LastName,
                        Picture = b.Creator.PictureId.AsProfilePicture(config),
                        ShowName = b.Creator.ShowName
                    },
                    Posts = b.Posts.OrderByDescending(p => p.Created)
                        .Select(p => new
                        {
                            Id = p.Id,
                            Title = p.Title,
                            Content = p.Content,
                            Created = p.Created,
                            Account = new
                            {
                                Username = p.Account.Username,
                                FirstName = p.Account.FirstName,
                                LastName = p.Account.LastName,
                                Picture = p.Account.PictureId.AsProfilePicture(config),
                                ShowName = p.Account.ShowName
                            },
                            Modified = p.Modified
                        })
                })
                .FirstOrDefaultAsync();

            if (blog != null)
                return Ok(blog);

            return NotFound();
        }

        [HttpPost, ApiPermission("BLOG_ADD")]
        public async Task<ActionResult> AddBlog(Blog blog)
        {
            var blogValidationResult = validateBlogRequest(blog);
            if (blogValidationResult != null)
                return blogValidationResult;

            blog.Created = DateTime.UtcNow;
            blog.CreatorId = HttpContext.User.GetAccountId();
            blog.Access = null;
            blog.Posts = null;

            try {
                this.dbContext.Add(blog);
                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't create a new blog.");
                return InternalError();
            }

            this.logger.LogInformation("Created new blog #{BlogId}: {BlogName} by {AccountId}", blog.Id, blog.Name, blog.CreatorId);
            return Ok(new { Id = blog.Id });
        }

        [HttpPatch("{blogId}"), ApiPermission("BLOG_EDIT")]
        public async Task<ActionResult> EditBlog([FromBody]Blog blogChanges, uint blogId)
        {
            var blog = await this.dbContext.Blogs.Where(b => b.Id == blogId)
                .Include(b => b.Creator)
                .FirstOrDefaultAsync();

            await RetrieveRoles();
            if (!UserCanTargetRole(blog))
                return Forbid();

            var blogValidationResult = validateBlogRequest(blogChanges);
            if (blogValidationResult != null)
                return blogValidationResult;

            blog.Name = blogChanges.Name;
            blog.Description = blogChanges.Description;
            blog.Visibility = blogChanges.Visibility;

            try {
                this.dbContext.Update(blog);
                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't update blog #{BlogId}", blogId);
                return InternalError();
            }

            this.logger.LogInformation("Edited blog #{BlogId}: {BlogName} by {AccountId}", blog.Id, blog.Name, blog.CreatorId);
            return Ok();
        }

        [HttpDelete("{blogId}"), ApiPermission("BLOG_DELETE")]
        public async Task<ActionResult> DeleteBlog(uint blogId)
        {
            var blog = await this.dbContext.Blogs.Where(b => b.Id == blogId)
                .Include(b => b.Creator)
                .FirstOrDefaultAsync();

            await RetrieveRoles();
            if (!UserCanTargetRole(blog))
                return Forbid();

            using (var transaction = await this.dbContext.Database.BeginTransactionAsync())
            {
                try {
                    this.dbContext.Comments.RemoveRange(this.dbContext.BlogPostComments.Where(c => c.BlogId == blogId).Select(c => c.Comment));
                    await this.dbContext.SaveChangesAsync();

                    this.dbContext.BlogPostComments.RemoveRange(this.dbContext.BlogPostComments.Where(c => c.BlogId == blogId));
                    await this.dbContext.SaveChangesAsync();

                    this.dbContext.BlogPosts.RemoveRange(this.dbContext.BlogPosts.Where(c => c.BlogId == blogId));
                    await this.dbContext.SaveChangesAsync();

                    this.dbContext.Blogs.Remove(blog);
                    await this.dbContext.SaveChangesAsync();

                    await transaction.CommitAsync();
                } catch (DbUpdateException ex) {
                    this.logger.LogError(ex, "Couldn't remove blog #{BlogId}", blogId);
                    await transaction.RollbackAsync();
                    return InternalError();
                }
            }

            this.logger.LogInformation("Deleted blog #{BlogId} by {AccountId}", blogId, blog.CreatorId);
            return Ok();
        }
#endregion

#region Posts
        [HttpGet("{blogId}/{postId}")]
        public async Task<ActionResult> GetPost(uint blogId, uint postId)
        {
            var post = await this.dbContext.BlogPosts
                .Include(p => p.Blog)
                    .ThenInclude(b => b.Creator)
                .Include(p => p.Comments)
                    .ThenInclude(c => c.Comment)
                        .ThenInclude(c => c.Account)
                .Include(p => p.Comments)
                        .ThenInclude(c => c.Comment)
                            .ThenInclude(c => c.Replies)
                .Where(p => p.BlogId == blogId && p.Id == postId
                    && (p.Blog.Visibility == Visibility.Public
                    || p.Blog.Visibility == Visibility.MembersOnly
                    || p.Blog.Access.Any(a => a.AccountId == HttpContext.User.GetAccountId() && a.Access >= Access.Read )))
                .Select(p => new
                {
                    Id = p.Id,
                    Title = p.Title,
                    Content = p.Content,
                    Account = p.AccountId.HasValue ? new
                    {
                        Username = p.Account.Username,
                        FirstName = p.Account.FirstName,
                        LastName = p.Account.LastName,
                        Picture = p.Account.PictureId.AsProfilePicture(config),
                        ShowName = p.Account.ShowName
                    } : null,
                    Comments = p.Comments.Select(c => new
                    {
                        Id = c.Comment.Id,
                        Created = c.Comment.Created,
                        Modified = c.Comment.Modified,
                        Account = new
                        {
                            Username = c.Comment.Account.Username,
                            FirstName = c.Comment.Account.FirstName,
                            LastName = c.Comment.Account.LastName,
                            Picture = c.Comment.Account.PictureId.AsProfilePicture(config),
                            ShowName = c.Comment.Account.ShowName
                        },
                        ReplyCount = c.Comment.Replies.Count()
                    })
                })
                .FirstOrDefaultAsync();

            if (post != null)
                return Ok(post);

            return NotFound();
        }

        [HttpPost("{blogId}/new"), ApiPermission("BLOG_POST_ADD")]
        public async Task<ActionResult> AddPost([FromBody]BlogPost post, uint blogId)
        {
            await this.RetrieveRoles();
            var blog = await this.dbContext.Blogs.Include(b => b.Creator)
                .FirstOrDefaultAsync(b => b.Id == blogId);

            await RetrieveRoles();
            if (!await UserCanTargetRole(blog, Access.Write))
                return Forbid();

            var postValidationResult = validatePostRequest(post);
            if (postValidationResult != null)
                return postValidationResult;

            post.AccountId = HttpContext.User.GetAccountId();
            post.BlogId = blogId;
            post.Id = await this.dbContext.BlogPosts.Select(a => a.Id).MaxAsync(x => (uint?)x) + 1 ?? 1;
            post.Created = DateTime.UtcNow;

            try
            {
                this.dbContext.Add(post);
                await this.dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                this.logger.LogError(ex, "Couldn't create a new blog post for #{BlogId}.", blogId);
                return InternalError();
            }

            this.logger.LogInformation("Created new blog post #{BlogId}.{PostId} by account {AccountId}", blogId, post.Id, HttpContext.User.GetAccountId());
            return Ok(new { Blog = blogId, Post = post.Id });
        }

        [HttpPatch("{blogId}/{postId}"), ApiPermission("BLOG_POST_EDIT")]
        public async Task<ActionResult> EditPost([FromBody]BlogPost post, uint blogId, uint postId)
        {
            var blogPost = await this.dbContext.BlogPosts
                .Include(p => p.Blog)
                    .ThenInclude(b => b.Creator)
                .Where(b => b.BlogId == blogId && b.Id == postId)
                .Select(b => new BlogPost
                {
                    Id = postId,
                    BlogId = blogId,
                    Blog = new Blog
                    {
                        Id = blogId,
                        Creator = new Account
                        {
                            Id = b.Blog.Creator.Id,
                            RoleId = b.Blog.Creator.RoleId
                        }
                    },
                    Title = b.Title,
                    Content = b.Content
                })
                .FirstOrDefaultAsync();


            await RetrieveRoles();
            if (!await UserCanTargetRole(blogPost.Blog, Access.Read))
                return Forbid();

            blogPost.Modified = DateTime.UtcNow;
            blogPost.Title = post.Title;
            blogPost.Content = post.Content;

            var postValidationResult = validatePostRequest(blogPost);
            if (postValidationResult != null)
                return postValidationResult;

            try {
                this.dbContext.BlogPosts.Update(blogPost);
                this.dbContext.Entry(blogPost.Account).State = EntityState.Unchanged;
                await this.dbContext.SaveChangesAsync();
            } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't create update blog post #{BlogId}.{PostId} for blog .", blogId, postId);
                return InternalError();
            }

            this.logger.LogInformation("Updated post #{BlogId}.{PostId} by {AccountId}", blogId, postId, HttpContext.User.GetAccountId());
            return Ok();
        }

        [HttpDelete("{blogId}/{postId}"), ApiPermission("BLOG_POST_DELETE")]
        public async Task<ActionResult> DeletePost(uint blogId, uint postId)
        {
            var blogPost = await this.dbContext.BlogPosts.Where(p => p.BlogId == blogId && p.Id == postId)
                .Include(p => p.Blog)
                    .ThenInclude(b => b.Creator)
                .Select(p => new
                {
                    Blog = new Blog
                    {
                        Id = blogId,
                        Creator = new Account
                        {
                            Id = p.Blog.Creator.Id,
                            RoleId = p.Blog.Creator.RoleId
                        }
                    }
                })
                .FirstOrDefaultAsync();

            await RetrieveRoles();
            if (!await UserCanTargetRole(blogPost.Blog, Access.Full))
                return Forbid();

            using (var transaction = await this.dbContext.Database.BeginTransactionAsync())
            {
                try {
                    this.dbContext.Comments.RemoveRange(this.dbContext.BlogPostComments
                        .Where(c =>c.BlogId == blogId && c.PostId == postId)
                        .Select(c => c.Comment));
                    await this.dbContext.SaveChangesAsync();

                    this.dbContext.BlogPostComments.RemoveRange(this.dbContext.BlogPostComments
                        .Where(c => c.BlogId == blogId && c.PostId == postId));
                    await this.dbContext.SaveChangesAsync();

                    this.dbContext.BlogPosts.RemoveRange(this.dbContext.BlogPosts
                        .Where(p => p.BlogId == blogId && p.Id == postId));
                    await this.dbContext.SaveChangesAsync();

                    await transaction.CommitAsync();
                } catch (DbUpdateException ex) {
                this.logger.LogError(ex, "Couldn't remove blog post #{BlogId}.{PostId}", blogId, postId);
                    await transaction.RollbackAsync();
                    return InternalError();
                }
            }

            this.logger.LogInformation("Removed blog post #{BlogId}.{PostId} by {AccountId}", blogId, postId, HttpContext.User.GetAccountId());
            return Ok();
        }
#endregion
        private IQueryable<Blog> getAuthedQueryable()
        {
            var query = this.dbContext.Blogs
                .Include(b => b.Creator);

            if (HttpContext.User.Identity.IsAuthenticated)
                return query.Where(b => b.Visibility == Visibility.Public
                    || b.Visibility == Visibility.MembersOnly
                    || b.Access.Any(a => a.AccountId == AccountId && a.Access >= Access.Read ));

            return query.Where(b => b.Visibility == Visibility.Public);
        }
        private ActionResult validateBlogRequest(Blog blog)
        {
            List<BadField> badFields = new List<BadField>();
            var config = this.config.GetSection("Limits:Blog");
            var response = ValidationTools.ValidateString(badFields,
                blog.Name, "name", config.GetSection("Name"), 2, 48);
            if (response != null)
                blog.Name = response;

            response = ValidationTools.ValidateString(badFields,
                blog.Description, "description", config.GetSection("Description"), 2, 255);
            if (response != null)
                blog.Description = response;

            if (badFields.Count > 0)
                return BadRequestResponse(badFields.ToArray());

            return null;
        }
        private ActionResult validatePostRequest(BlogPost post)
        {
            List<BadField> badFields = new List<BadField>();
            var config = this.config.GetSection("Limits:BlogPost");
            var response = ValidationTools.ValidateString(badFields,
                post.Title, "title", config.GetSection("Title"), 2, 128);
            if (response != null)
                post.Title = response;

            response = ValidationTools.ValidateString(badFields,
                post.Content, "content", config.GetSection("Content"), 2, 65535);
            if (response != null)
                post.Content = response;

            if (badFields.Count > 0)
                return BadRequestResponse(badFields.ToArray());

            return null;
        }

        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public bool UserCanTargetRole(Blog blog)
                => (blog != null && UserCanTargetRole(blog.Creator.RoleId));

        [NonAction]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<bool> UserCanTargetRole(Blog blog, Access access)
                => (blog != null
                    && (UserCanTargetRole(blog.Creator.RoleId)
                        || await this.dbContext.BlogAccesses.Include(a => a.Blog)
                            .AnyAsync(a => a.BlogId == blog.Id &&
                                (a.Blog.Creator.Id == AccountId
                                    || ((byte)a.Access >= (byte)access && a.AccountId == AccountId)))));
    }
}