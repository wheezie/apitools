using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;

using Wheezie.ApiTools.Business.Authentication;
using Wheezie.ApiTools.Business.Authorization;

namespace Wheezie.ApiTools.Extensions
{
    public static class ApiAuthenticationExtensions
    {
        /// <summary>
        /// Register generic api authentication
        /// </summary>
        /// <param name="services">Source DI IServiceCollectionn</param>
        /// <param name="authOptions">Application collection</param>
        /// <returns>The modified DI IServiceCollection</returns>
        public static IServiceCollection AddApiAuthentication(this IServiceCollection services, Action<ApiAuthOptions> authOptions)
        {
            services.AddSingleton<IAuthorizationPolicyProvider, ApiAuthPolicyProvider>();
            services.AddScoped<IAuthorizationHandler, ApiAuthorizationHandler>();

            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = ApiAuthOptions.DefaultScheme;
                o.DefaultChallengeScheme = ApiAuthOptions.DefaultScheme;
            }).AddScheme<ApiAuthOptions, ApiAuthHandler>(ApiAuthOptions.DefaultScheme, authOptions);
            return services;
        }
    }
}