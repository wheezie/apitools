using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Wheezie.ApiTools.EF;
using Wheezie.ApiTools.Mail;

namespace Wheezie.ApiTools.Extensions
{
    public static class ApiBasicExtensions
    {
        /// <summary>
        /// Include ApiTools basics
        /// </summary>
        public static IServiceCollection AddApiTools(this IServiceCollection services, IConfiguration config)
        {
            // DI
            services.AddSingleton<PasswordHasher>();
            services.AddSingleton<IApiToolsMail, ApiToolsMail>();

            // Detection
            services.AddDetection();

            // Database
            string connectionString = config.GetConnectionString("ApiDbContext");
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException("Connectionstring 'ApiDbContext' must be configured!");
            else
                services.AddDbContext<AppDbContext>(o => o.UseMySql(connectionString));
            return services;
        }

        /// <summary>
        /// Include swagger api documentation generator
        /// </summary>
        /// <param name="services"></param>
        /// <param name="name">Documentation name</param>
        /// <param name="version">Version identifier</param>
        /// <param name="license">License name</param>
        /// <param name="licenseUrl">License url</param>
        public static IServiceCollection AddApiSwagger(this IServiceCollection services, string name, string version, string license = "MIT", string licenseUrl = "https://opensource.org/licenses/MIT")
            => services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc(
                        version,
                        new Microsoft.OpenApi.Models.OpenApiInfo
                        {
                            Title = name,
                            Version = version,
                            License = new Microsoft.OpenApi.Models.OpenApiLicense
                            {
                                Name = license,
                                Url = new Uri(licenseUrl)
                            }
                        });

                    c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.xml"));
                });

        /// <summary>
        /// Use ApiTools basics
        /// </summary>
        public static IApplicationBuilder UseApiTools(this IApplicationBuilder builder)
        {
            builder.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
            });

            builder.UseExceptionHandler("/error/500");

            using (var scope = builder.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            using (var context = scope.ServiceProvider.GetService<AppDbContext>())
                context.Database.Migrate();

            return builder;
        }

        /// <summary>
        /// Use the swagger implementation and the swagger UI
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="version">API version identifier</param>
        /// <param name="name">API Name</param>
        /// <param name="route">API route prefix</param>
        /// <returns></returns>
        public static IApplicationBuilder UseApiSwagger(this IApplicationBuilder builder, string version, string name, string route = "doc")
            => builder.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"/swagger/{version}/swagger.json", name);
                    c.RoutePrefix = route;
                });
    }
}