using System;

namespace Wheezie.ApiTools.EF.Models.Generic
{
    public class ContentBase
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        public ulong? AccountId { get; set; }
        /// <summary>
        /// Content (parsed)
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Created timestamp
        /// </summary>
        public DateTime Created { get; set; }
        /// <summary>
        /// Modified timestamp
        /// </summary>
        public DateTime? Modified { get; set; }


        /// <summary>
        /// Account
        /// </summary>
        public virtual Account Account { get; set; }
    }
}