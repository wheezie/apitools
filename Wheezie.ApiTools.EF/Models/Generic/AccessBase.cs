namespace Wheezie.ApiTools.EF.Models.Generic
{
    public class AccessBase
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        public ulong AccountId { get; set; }
        /// <summary>
        /// Access list
        /// </summary>
        public Access Access { get; set; }


        /// <summary>
        /// Account
        /// </summary>
        public Account Account { get; set; }
    }
}