using System;

namespace Wheezie.ApiTools.EF.Models.Generic
{
    public class Post : ContentBase
    {
        /// <summary>
        /// Post identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Visibility state
        /// </summary>
        public Visibility Visibility { get; set; }
    }
}