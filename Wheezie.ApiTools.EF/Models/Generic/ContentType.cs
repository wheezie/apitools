namespace Wheezie.ApiTools.EF.Models.Generic
{
    public enum ContentType : byte
    {
        None,
        Video,
        Image
    }
}