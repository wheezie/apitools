using System;

namespace Wheezie.ApiTools.EF.Models
{
    public class AccountPostPicture
    {
        /// <summary>
        /// Post identifier
        /// </summary>
        /// <value></value>
        public uint PostId { get; set; }
        /// <summary>
        /// Account identifier
        /// </summary>
        /// <value></value>
        public ulong AccountId { get; set; }
        /// <summary>
        /// Picture identifier
        /// </summary>
        public Guid PictureId { get; set; }


        /// <summary>
        /// Account post
        /// </summary>
        public virtual AccountPost Post { get; set; }
        /// <summary>
        /// Picture
        /// </summary>
        public virtual Picture Picture { get; set; }
        /// <summary>
        /// Account
        /// </summary>
        public virtual Account Account { get; set; }
    }
}