using System.Collections.Generic;

using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class Event
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Event name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Event description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Promotion url
        /// </summary>
        public string Promotion { get; set; }
        /// <summary>
        /// Promotion type
        /// </summary>
        public ContentType PromotionType { get; set; }
        /// <summary>
        /// Account identifier
        /// </summary>
        public ulong AccountId { get; set; }

        /// <summary>
        /// Occurance dates
        /// </summary>
        public List<EventDate> Dates { get; set; }
            = new List<EventDate>();
        /// <summary>
        /// Crew members
        /// </summary>
        public List<EventCrew> Crews { get; set; }
            = new List<EventCrew>();

        /// <summary>
        /// Account
        /// </summary>
        public virtual Account Account { get; set; }
    }
}