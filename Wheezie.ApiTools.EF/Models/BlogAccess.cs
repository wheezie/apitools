using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class BlogAccess : AccessBase
    {
        /// <summary>
        /// Blog identifier
        /// </summary>
        public uint BlogId { get; set; }


        /// <summary>
        /// Blog
        /// </summary>
        public virtual Blog Blog { get; set; }
    }
}