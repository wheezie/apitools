using System;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Configuration;

namespace Wheezie.ApiTools.EF.Models
{
    public class Picture
    {
        /// <summary>
        /// Picture identifier
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Uploaded timestamp
        /// </summary>
        public DateTime Uploaded { get; set; }
        /// <summary>
        /// Uploader identifier
        /// </summary>
        public ulong? UploaderId { get; set; }
        /// <summary>
        /// Current state
        /// </summary>
        public PictureState State { get; set; }
        /// <summary>
        /// State changed timestamp
        /// </summary>
        /// <value></value>
        public DateTime? StateChanged { get; set; }


        /// <summary>
        /// Uploader
        /// </summary>
        [JsonIgnore]
        public virtual Account Uploader { get; set; }

        public string GetPath(IConfigurationSection config) => $"{config.GetValue<string>("Paths:Access:Pictures")}{Id.ToString("N")}";
    }

    public enum PictureState : byte
    {
        Public = 0,
        MemberOnly = 1,
        Private = 2,
        Deleted = 3
    }
}