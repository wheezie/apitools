using System;

namespace Wheezie.ApiTools.EF.Models
{
    public class AccountEmail
    {
        /// <summary>
        /// Email address (unique, key)
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Account identifier
        /// </summary>
        public ulong? AccountId { get; set; }
        /// <summary>
        /// Address type
        /// </summary>
        public EmailType Type { get; set; }
        /// <summary>
        /// Verified state
        /// </summary>
        public bool Verified { get; set; }


        /// <summary>
        /// Account linked to the email address by the AccountId
        /// </summary>
        public virtual Account Account { get; set; }
    }


    public enum EmailType : byte
    {
        // Banned address
        Banned = 0,
        /// Primary account address
        Primary = 1,
        /// Additional address
        Secondary = 2,
        /// Recovery address
        Recovery = 3
    }
}