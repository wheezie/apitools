using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Wheezie.ApiTools.EF.Models
{
    public class Album
    {
        /// <summary>
        /// Album identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Account identifier
        /// </summary>
        public ulong? AccountId { get; set; }
        /// <summary>
        /// Album name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Album description
        /// </summary>
        public string Description { get; set; }
        [JsonIgnore]
        public List<AlbumPicture> Pictures { get; set; }
            = new List<AlbumPicture>();
        [JsonIgnore]
        public List<AlbumLike> Likes { get; set; }
            = new List<AlbumLike>();


        /// <summary>
        /// Account
        /// </summary>
        [JsonIgnore]
        public virtual Account Account { get; set; }
    }
}