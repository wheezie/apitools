using System;

namespace Wheezie.ApiTools.EF.Models
{
    public class Session
    {
        /// <summary>
        /// Session token
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Account identifier
        /// </summary>
        public ulong? AccountId { get; set; }
        /// <summary>
        /// Browser type
        /// </summary>
        public Browser Browser { get; set; }
        /// <summary>
        /// Platform type
        /// </summary>
        public Platform Platform { get; set; }
        /// <summary>
        /// Session issued date
        /// </summary>
        public DateTime Issued { get; set; }
        /// <summary>
        /// Last token use
        /// </summary>
        public DateTime? LastUse { get; set; }
        /// <summary>
        /// Expire
        /// </summary>
        public DateTime Expires { get; set; }
        /// <summary>
        /// Issued ip address
        /// </summary>
        public string Ip { get; set; }
        /// <summary>
        /// Last ip address
        /// </summary>
        public string LastIp { get; set; }


        /// <summary>
        /// Account
        /// </summary>
        public virtual Account Account { get; set; }
    }
}