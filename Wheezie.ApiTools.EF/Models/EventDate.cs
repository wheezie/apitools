using System;

namespace Wheezie.ApiTools.EF.Models
{
    public class EventDate
    {
        /// <summary>
        /// Event identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Event
        /// </summary>
        public virtual Event Event { get; set; }
    }
}