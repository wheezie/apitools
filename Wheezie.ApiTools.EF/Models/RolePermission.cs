using System.Text.Json.Serialization;

namespace Wheezie.ApiTools.EF.Models
{
    public class RolePermission
    {
        /// <summary>
        /// Permission role identifier
        /// </summary>
        public uint RoleId { get; set; }
        /// <summary>
        /// Permission identifier
        /// </summary>
        public string Permission { get; set; }
        /// <summary>
        /// Permission targetable role identifier
        /// </summary>
        public uint? CanTargetId { get; set; }


        /// <summary>
        /// Permission role
        /// </summary>
        [JsonIgnore]
        public virtual Role Role { get; set; }
        /// <summary>
        /// Permission targetable role
        /// </summary>
        [JsonIgnore]
        public virtual Role CanTarget { get; set; }
    }
}