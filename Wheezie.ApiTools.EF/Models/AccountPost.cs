using System.Collections.Generic;

using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class AccountPost : Post
    {
        /// <summary>
        /// Pictures
        /// </summary>
        public List<AccountPostPicture> Pictures { get; set; }
            = new List<AccountPostPicture>();
        /// <summary>
        /// Comments
        /// </summary>
        public List<AccountPostComment> Comments { get; set; }
            = new List<AccountPostComment>();
        /// <summary>
        /// Likes
        /// </summary>
        public List<AccountPostLike> Likes { get; set; }
            = new List<AccountPostLike>();
    }
}