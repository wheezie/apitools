using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class AccountPostLike : LikeBase
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        public ulong AccountId { get; set; }
        /// <summary>
        /// Post identifier
        /// </summary>
        public uint PostId { get; set; }



        /// <summary>
        /// Account
        /// </summary>
        public virtual Account Account { get; set; }
        /// <summary>
        /// Post
        /// </summary>
        public virtual AccountPost Post { get; set; }
    }
}