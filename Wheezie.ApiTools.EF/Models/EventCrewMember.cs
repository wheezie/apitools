namespace Wheezie.ApiTools.EF.Models
{
    public class EventCrewMember
    {
        /// <summary>
        /// Event identifier
        /// </summary>
        /// <value></value>
        public uint Id { get; set; }
        /// <summary>
        /// Crew identifier
        /// </summary>
        public uint CrewId { get; set; }
        /// <summary>
        /// Crew title
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Crew member name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Event
        /// </summary>
        public virtual Event Event { get; set; }
        /// <summary>
        /// Crew event
        /// </summary>
        public virtual EventCrew Crew { get; set; }
    }
}