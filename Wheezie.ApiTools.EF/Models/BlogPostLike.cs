using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class BlogPostLike : LikeBase
    {
        /// <summary>
        /// Post identifier
        /// </summary>
        public uint PostId { get; set; }


        /// <summary>
        /// Post
        /// </summary>
        public virtual BlogPost Post { get; set; }
    }
}