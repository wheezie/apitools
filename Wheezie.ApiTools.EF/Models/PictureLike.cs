using System;

namespace Wheezie.ApiTools.EF.Models
{
    public class PictureLike
    {
        public Guid PictureId { get; set; }
        public ulong AccountId { get; set; }
        public DateTime Date { get; set; }
        public bool Unliked { get; set; }


        public virtual Picture Picture { get; set; }
        public virtual Account Account { get; set; }
    }
}