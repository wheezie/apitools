using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class AccountPostComment
    {
        /// <summary>
        /// Comment identifier
        /// </summary>
        public ulong CommentId { get; set; }
        /// <summary>
        /// Post identifier
        /// </summary>
        public uint PostId { get; set; }
        /// <summary>
        /// Post account identifier
        /// </summary>
        public ulong AccountId { get; set; }


        public virtual Comment Comment { get; set; }
        /// <summary>
        /// Post
        /// </summary>
        public virtual AccountPost Post { get; set; }
        /// <summary>
        /// Poster
        /// </summary>
        public virtual Account Account { get; set; }
    }
}