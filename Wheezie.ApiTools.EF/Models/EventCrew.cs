using System.Collections.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class EventCrew
    {
        /// <summary>
        /// Event identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Crew identifier
        /// </summary>
        public uint CrewId { get; set; }
        /// <summary>
        /// Crew list name
        /// </summary>
        public string Name { get; set; }
        public List<EventCrewMember> Members { get; set; }
            = new List<EventCrewMember>();

        /// <summary>
        /// Crew event
        /// </summary>
        public virtual Event Event { get; set; }
    }
}