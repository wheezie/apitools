using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class AlbumLike : LikeBase
    {
        /// <summary>
        /// Album identifier
        /// </summary>
        public uint Id { get; set; }


        /// <summary>
        /// Album
        /// </summary>
        public virtual Album Album { get; set; }
    }
}