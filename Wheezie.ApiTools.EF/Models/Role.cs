using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Wheezie.ApiTools.EF.Models
{
    /// <summary>
    /// Administrative & permissive role
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Role Identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Role name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Role description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Disabled state, if enabled role will be unavailable & defaulted.
        /// </summary>
        public bool Disabled { get; set; }
        /// <summary>
        /// Targetable Role identifier
        /// </summary>
        public uint? CanTargetId { get; set; }
        /// <summary>
        /// Permissions
        /// </summary>
        public List<RolePermission> Permissions { get; set; }
            = new List<RolePermission>();
        /// <summary>
        /// Members
        /// </summary>
        /// <value></value>
        [JsonIgnore]
        public List<Account> Members { get; set; }
            = new List<Account>();


        /// <summary>
        /// Targetable role linked by the CanTargetId
        /// </summary>
        [JsonIgnore]
        public virtual Role CanTarget { get; set; }
    }
}