using System;
using System.Collections.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class Account
    {
        /// <summary>
        /// Account administrative identifier
        /// </summary>
        public ulong Id  { get; set; }
        /// <summary>
        /// Username (unique)
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Password hash
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// First name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Full account name
        /// </summary>
        public string FullName { get { return $"{this.FirstName} {this.LastName}"; } }
        /// <summary>
        /// Description (may be used for administrative purposes)
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Public bio
        /// </summary>
        public string Bio { get; set; }
        /// <summary>
        /// Website
        /// </summary>
        public string Website { get; set; }
        /// <summary>
        /// Email address
        /// </summary>
        public string ProfileEmailId { get; set; }
        /// <summary>
        /// Name visibility
        /// </summary>
        public Visibility ShowName { get; set; }
        /// <summary>
        /// Email visibility
        /// </summary>
        public Visibility ShowEmail { get; set; }
        /// <summary>
        /// Website visibility
        /// </summary>
        public Visibility ShowWebsite { get; set; }
        /// <summary>
        /// Account picture (if any)
        /// </summary>
        public Guid? PictureId { get; set; }
        /// <summary>
        /// Registration date
        /// </summary>
        public DateTime RegisteredDate { get; set; }
        /// <summary>
        /// Account state
        /// </summary>
        public AccountState State { get; set; }
        /// <summary>
        /// Blocked datestamp
        /// </summary>
        public DateTime? BlockedDate { get; set; }
        /// <summary>
        /// Role identifier
        /// </summary>
        public uint RoleId { get; set; }
        /// <summary>
        /// Failed authentication attempts
        /// </summary>
        public byte FailedAuths { get; set; }
        /// <summary>
        /// Last failed authentication attempt
        /// </summary>
        public DateTime? LastFailedAuth { get; set; }
        /// <summary>
        /// Corresponding email address list
        /// </summary>
        public List<AccountEmail> Emails { get; set; }
            = new List<AccountEmail>();
        /// <summary>
        /// Created invite tokens
        /// </summary>
        public List<AccountInvite> Invites { get; set; }
            = new List<AccountInvite>();
        /// <summary>
        /// Associated posts
        /// </summary>
        /// <value></value>
        public List<AccountPost> Posts { get; set; }
            = new List<AccountPost>();
        /// <summary>
        /// Albums
        /// </summary>
        public List<Album> Albums { get; set; }
            = new List<Album>();
        /// <summary>
        /// Created blogs
        /// </summary>
        public List<Blog> Blogs { get; set; }
            = new List<Blog>();
        /// <summary>
        /// Blog access grants
        /// </summary>
        public List<BlogAccess> BlogAccess { get; set; }
            = new List<BlogAccess>();
        /// <summary>
        /// Posted blogposts
        /// </summary>
        public List<BlogPost> BlogPosts { get; set; }
            = new List<BlogPost>();
        /// <summary>
        /// Linked sessions
        /// </summary>
        public List<Session> Sessions { get; set; }
            = new List<Session>();


        /// <summary>
        /// Account role
        /// </summary>
        public virtual Role Role { get; set; }
        /// <summary>
        /// Account invite
        /// </summary>
        public virtual AccountInvite Invite { get; set; }
        /// <summary>
        /// Profile email
        /// </summary>
        public virtual AccountEmail ProfileEmail { get; set; }
        /// <summary>
        /// Account picture
        /// </summary>
        public virtual Picture Picture { get; set; }
    }

    public enum AccountState : byte
    {
        // Unconfirmed account is banned
        Banned = 0,
        // Unconfirmed account blocked
        Blocked = 1,
        // Account unconfirmed
        Unconfirmed = 2,
        // Confirmed account was banned
        ConfirmedBanned = 100,
        // Confirmed account is Blocked
        ConfirmedBlocked = 101,
        // Confirmed
        Confirmed = 102,

        PendingRemoval = 255
    }
}