using System.Collections.Generic;

using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class Comment : ContentBase
    {
        /// <summary>
        /// Comment identifier
        /// </summary>
        public ulong Id { get; set; }
        /// <summary>
        /// Replies
        /// </summary>
        public List<CommentReply> Replies { get; set; }
    }
}