using System.Collections.Generic;

using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.EF.Models
{
    public class BlogPost : Post
    {
        /// <summary>
        /// Blog identifier
        /// </summary>
        public uint BlogId { get; set; }
        /// <summary>
        /// Comments
        /// </summary>
        public List<BlogPostComment> Comments { get; set; }
            = new List<BlogPostComment>();


        /// <summary>
        /// Blog
        /// </summary>
        /// <value></value>
        public virtual Blog Blog { get; set;}
    }
}