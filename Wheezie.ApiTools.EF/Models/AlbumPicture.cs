using System;

namespace Wheezie.ApiTools.EF.Models
{
    public class AlbumPicture
    {
        /// <summary>
        /// Album identifier
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Picture identifier
        /// </summary>
        public Guid PictureId { get; set; }


        /// <summary>
        /// Album
        /// </summary>
        public Album Album { get; set; }
        /// <summary>
        /// Picture
        /// </summary>
        public Picture Picture { get; set; }
    }
}