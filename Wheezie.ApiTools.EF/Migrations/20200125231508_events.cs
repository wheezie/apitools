﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wheezie.ApiTools.EF.Migrations
{
    public partial class events : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    Id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    Description = table.Column<string>(maxLength: 65535, nullable: false),
                    Promotion = table.Column<string>(maxLength: 255, nullable: true),
                    PromotionType = table.Column<byte>(nullable: false),
                    AccountId = table.Column<ulong>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Events_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EventCrews",
                columns: table => new
                {
                    Id = table.Column<uint>(nullable: false),
                    CrewId = table.Column<uint>(nullable: false),
                    Name = table.Column<string>(maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventCrews", x => new { x.Id, x.CrewId });
                    table.ForeignKey(
                        name: "FK_EventCrews_Events_Id",
                        column: x => x.Id,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EventDates",
                columns: table => new
                {
                    Id = table.Column<uint>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventDates", x => new { x.Id, x.Date });
                    table.ForeignKey(
                        name: "FK_EventDates_Events_Id",
                        column: x => x.Id,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EventCrewMembers",
                columns: table => new
                {
                    Id = table.Column<uint>(nullable: false),
                    CrewId = table.Column<uint>(nullable: false),
                    Title = table.Column<string>(maxLength: 32, nullable: false),
                    Name = table.Column<string>(maxLength: 65, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventCrewMembers", x => x.Id);
                    table.UniqueConstraint("AK_EventCrewMembers_Title", x => x.Title);
                    table.ForeignKey(
                        name: "FK_EventCrewMembers_Events_Id",
                        column: x => x.Id,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EventCrewMembers_EventCrews_Id_CrewId",
                        columns: x => new { x.Id, x.CrewId },
                        principalTable: "EventCrews",
                        principalColumns: new[] { "Id", "CrewId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "RegisteredDate", "Role" },
                values: new object[] { new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1u });

            migrationBuilder.CreateIndex(
                name: "IX_EventCrewMembers_Id_CrewId",
                table: "EventCrewMembers",
                columns: new[] { "Id", "CrewId" });

            migrationBuilder.CreateIndex(
                name: "IX_Events_AccountId",
                table: "Events",
                column: "AccountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EventCrewMembers");

            migrationBuilder.DropTable(
                name: "EventDates");

            migrationBuilder.DropTable(
                name: "EventCrews");

            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "RegisteredDate", "Role" },
                values: new object[] { new DateTime(2020, 1, 16, 21, 56, 55, 251, DateTimeKind.Utc).AddTicks(6178), 1u });
        }
    }
}
