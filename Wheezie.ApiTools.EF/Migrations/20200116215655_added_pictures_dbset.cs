﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wheezie.ApiTools.EF.Migrations
{
    public partial class added_pictures_dbset : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountPostPictures_Picture_Picture",
                table: "AccountPostPictures");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_Picture_Picture",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_AlbumPictures_Picture_Picture",
                table: "AlbumPictures");

            migrationBuilder.DropForeignKey(
                name: "FK_Picture_Accounts_Uploader",
                table: "Picture");

            migrationBuilder.DropForeignKey(
                name: "FK_PictureLike_Accounts_Account",
                table: "PictureLike");

            migrationBuilder.DropForeignKey(
                name: "FK_PictureLike_Picture_Picture",
                table: "PictureLike");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PictureLike",
                table: "PictureLike");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Picture",
                table: "Picture");

            migrationBuilder.RenameTable(
                name: "PictureLike",
                newName: "PictureLikes");

            migrationBuilder.RenameTable(
                name: "Picture",
                newName: "Pictures");

            migrationBuilder.RenameIndex(
                name: "IX_PictureLike_Account",
                table: "PictureLikes",
                newName: "IX_PictureLikes_Account");

            migrationBuilder.RenameIndex(
                name: "IX_Picture_Uploader",
                table: "Pictures",
                newName: "IX_Pictures_Uploader");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PictureLikes",
                table: "PictureLikes",
                columns: new[] { "Picture", "Account" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Pictures",
                table: "Pictures",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "RegisteredDate", "Role" },
                values: new object[] { new DateTime(2020, 1, 16, 21, 56, 55, 251, DateTimeKind.Utc).AddTicks(6178), 1u });

            migrationBuilder.AddForeignKey(
                name: "FK_AccountPostPictures_Pictures_Picture",
                table: "AccountPostPictures",
                column: "Picture",
                principalTable: "Pictures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_Pictures_Picture",
                table: "Accounts",
                column: "Picture",
                principalTable: "Pictures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AlbumPictures_Pictures_Picture",
                table: "AlbumPictures",
                column: "Picture",
                principalTable: "Pictures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PictureLikes_Accounts_Account",
                table: "PictureLikes",
                column: "Account",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PictureLikes_Pictures_Picture",
                table: "PictureLikes",
                column: "Picture",
                principalTable: "Pictures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Pictures_Accounts_Uploader",
                table: "Pictures",
                column: "Uploader",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountPostPictures_Pictures_Picture",
                table: "AccountPostPictures");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_Pictures_Picture",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_AlbumPictures_Pictures_Picture",
                table: "AlbumPictures");

            migrationBuilder.DropForeignKey(
                name: "FK_PictureLikes_Accounts_Account",
                table: "PictureLikes");

            migrationBuilder.DropForeignKey(
                name: "FK_PictureLikes_Pictures_Picture",
                table: "PictureLikes");

            migrationBuilder.DropForeignKey(
                name: "FK_Pictures_Accounts_Uploader",
                table: "Pictures");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Pictures",
                table: "Pictures");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PictureLikes",
                table: "PictureLikes");

            migrationBuilder.RenameTable(
                name: "Pictures",
                newName: "Picture");

            migrationBuilder.RenameTable(
                name: "PictureLikes",
                newName: "PictureLike");

            migrationBuilder.RenameIndex(
                name: "IX_Pictures_Uploader",
                table: "Picture",
                newName: "IX_Picture_Uploader");

            migrationBuilder.RenameIndex(
                name: "IX_PictureLikes_Account",
                table: "PictureLike",
                newName: "IX_PictureLike_Account");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Picture",
                table: "Picture",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PictureLike",
                table: "PictureLike",
                columns: new[] { "Picture", "Account" });

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "RegisteredDate", "Role" },
                values: new object[] { new DateTime(2020, 1, 12, 11, 53, 36, 498, DateTimeKind.Utc).AddTicks(8202), 1u });

            migrationBuilder.AddForeignKey(
                name: "FK_AccountPostPictures_Picture_Picture",
                table: "AccountPostPictures",
                column: "Picture",
                principalTable: "Picture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_Picture_Picture",
                table: "Accounts",
                column: "Picture",
                principalTable: "Picture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AlbumPictures_Picture_Picture",
                table: "AlbumPictures",
                column: "Picture",
                principalTable: "Picture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Picture_Accounts_Uploader",
                table: "Picture",
                column: "Uploader",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PictureLike_Accounts_Account",
                table: "PictureLike",
                column: "Account",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PictureLike_Picture_Picture",
                table: "PictureLike",
                column: "Picture",
                principalTable: "Picture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
