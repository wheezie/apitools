﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wheezie.ApiTools.EF.Migrations
{
    public partial class invite_relationfix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountInvite_Accounts_Acceptor",
                table: "AccountInvite");

            migrationBuilder.DropColumn(
                name: "Invite",
                table: "Accounts");

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "BlockedDate", "RegisteredDate", "State" },
                values: new object[] { null, new DateTime(2019, 12, 22, 17, 19, 23, 450, DateTimeKind.Utc).AddTicks(5502), (byte)101 });

            migrationBuilder.AddForeignKey(
                name: "FK_AccountInvite_Accounts_Acceptor",
                table: "AccountInvite",
                column: "Acceptor",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountInvite_Accounts_Acceptor",
                table: "AccountInvite");

            migrationBuilder.AddColumn<Guid>(
                name: "Invite",
                table: "Accounts",
                type: "char(36)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "BlockedDate", "RegisteredDate", "State" },
                values: new object[] { new DateTime(2019, 12, 22, 13, 0, 45, 705, DateTimeKind.Utc).AddTicks(1563), new DateTime(2019, 12, 22, 13, 0, 45, 705, DateTimeKind.Utc).AddTicks(1205), (byte)0 });

            migrationBuilder.AddForeignKey(
                name: "FK_AccountInvite_Accounts_Acceptor",
                table: "AccountInvite",
                column: "Acceptor",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
