﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wheezie.ApiTools.EF.Migrations
{
    public partial class admin_role_targets_user : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                column: "Role",
                value: 1u);

            migrationBuilder.UpdateData(
                table: "RolePermissions",
                keyColumns: new[] { "Role", "Permission" },
                keyValues: new object[] { 1u, "*" },
                column: "CanTarget",
                value: 2u);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                column: "Role",
                value: 1u);

            migrationBuilder.UpdateData(
                table: "RolePermissions",
                keyColumns: new[] { "Role", "Permission" },
                keyValues: new object[] { 1u, "*" },
                column: "CanTarget",
                value: 1u);
        }
    }
}
