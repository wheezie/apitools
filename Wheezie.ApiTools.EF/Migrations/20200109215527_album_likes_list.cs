﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wheezie.ApiTools.EF.Migrations
{
    public partial class album_likes_list : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "RegisteredDate", "Role" },
                values: new object[] { new DateTime(2020, 1, 9, 21, 55, 26, 409, DateTimeKind.Utc).AddTicks(7471), 1u });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "RegisteredDate", "Role" },
                values: new object[] { new DateTime(2019, 12, 24, 20, 6, 44, 944, DateTimeKind.Utc).AddTicks(9907), 1u });
        }
    }
}
