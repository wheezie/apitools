﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wheezie.ApiTools.EF.Migrations
{
    public partial class ignore_path : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AlbumLike_Albums_Id",
                table: "AlbumLike");

            migrationBuilder.DropForeignKey(
                name: "FK_AlbumLike_Accounts_Like",
                table: "AlbumLike");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AlbumLike",
                table: "AlbumLike");

            migrationBuilder.DropColumn(
                name: "Path",
                table: "Picture");

            migrationBuilder.RenameTable(
                name: "AlbumLike",
                newName: "AlbumLikes");

            migrationBuilder.RenameIndex(
                name: "IX_AlbumLike_Like",
                table: "AlbumLikes",
                newName: "IX_AlbumLikes_Like");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Albums",
                maxLength: 48,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Albums",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_AlbumLikes",
                table: "AlbumLikes",
                columns: new[] { "Id", "Like" });

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "RegisteredDate", "Role" },
                values: new object[] { new DateTime(2020, 1, 12, 11, 53, 36, 498, DateTimeKind.Utc).AddTicks(8202), 1u });

            migrationBuilder.AddForeignKey(
                name: "FK_AlbumLikes_Albums_Id",
                table: "AlbumLikes",
                column: "Id",
                principalTable: "Albums",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AlbumLikes_Accounts_Like",
                table: "AlbumLikes",
                column: "Like",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AlbumLikes_Albums_Id",
                table: "AlbumLikes");

            migrationBuilder.DropForeignKey(
                name: "FK_AlbumLikes_Accounts_Like",
                table: "AlbumLikes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AlbumLikes",
                table: "AlbumLikes");

            migrationBuilder.RenameTable(
                name: "AlbumLikes",
                newName: "AlbumLike");

            migrationBuilder.RenameIndex(
                name: "IX_AlbumLikes_Like",
                table: "AlbumLike",
                newName: "IX_AlbumLike_Like");

            migrationBuilder.AddColumn<string>(
                name: "Path",
                table: "Picture",
                type: "varchar(255) CHARACTER SET utf8mb4",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Albums",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 48);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Albums",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_AlbumLike",
                table: "AlbumLike",
                columns: new[] { "Id", "Like" });

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "RegisteredDate", "Role" },
                values: new object[] { new DateTime(2020, 1, 9, 21, 55, 26, 409, DateTimeKind.Utc).AddTicks(7471), 1u });

            migrationBuilder.AddForeignKey(
                name: "FK_AlbumLike_Albums_Id",
                table: "AlbumLike",
                column: "Id",
                principalTable: "Albums",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AlbumLike_Accounts_Like",
                table: "AlbumLike",
                column: "Like",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
