﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wheezie.ApiTools.EF.Migrations
{
    public partial class initial_migrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 32, nullable: false),
                    Description = table.Column<string>(maxLength: 512, nullable: true),
                    Disabled = table.Column<bool>(nullable: false),
                    CanTarget = table.Column<uint>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Roles_Roles_CanTarget",
                        column: x => x.CanTarget,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RolePermissions",
                columns: table => new
                {
                    Role = table.Column<uint>(nullable: false),
                    Permission = table.Column<string>(nullable: false),
                    CanTarget = table.Column<uint>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RolePermissions", x => new { x.Role, x.Permission });
                    table.ForeignKey(
                        name: "FK_RolePermissions_Roles_CanTarget",
                        column: x => x.CanTarget,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RolePermissions_Roles_Role",
                        column: x => x.Role,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(maxLength: 32, nullable: false),
                    Password = table.Column<string>(fixedLength: true, maxLength: 60, nullable: false),
                    FirstName = table.Column<string>(maxLength: 32, nullable: true),
                    LastName = table.Column<string>(maxLength: 32, nullable: true),
                    Description = table.Column<string>(maxLength: 1024, nullable: true),
                    Bio = table.Column<string>(maxLength: 255, nullable: true),
                    Website = table.Column<string>(maxLength: 255, nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    ShowName = table.Column<byte>(nullable: false),
                    ShowEmail = table.Column<byte>(nullable: false),
                    ShowWebsite = table.Column<byte>(nullable: false),
                    Picture = table.Column<Guid>(nullable: true),
                    RegisteredDate = table.Column<DateTime>(nullable: false),
                    State = table.Column<byte>(nullable: false),
                    BlockedDate = table.Column<DateTime>(nullable: true),
                    Role = table.Column<uint>(nullable: true),
                    Invite = table.Column<Guid>(nullable: true),
                    FailedAuths = table.Column<byte>(nullable: false),
                    LastFailedAuth = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Accounts_Roles_Role",
                        column: x => x.Role,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountEmails",
                columns: table => new
                {
                    Email = table.Column<string>(maxLength: 255, nullable: false),
                    Account = table.Column<ulong>(nullable: true),
                    Type = table.Column<byte>(nullable: false),
                    Verified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountEmails", x => x.Email);
                    table.ForeignKey(
                        name: "FK_AccountEmails_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "AccountInvite",
                columns: table => new
                {
                    Token = table.Column<Guid>(nullable: false),
                    Inviter = table.Column<ulong>(nullable: true),
                    Invited = table.Column<DateTime>(nullable: false),
                    Expire = table.Column<DateTime>(nullable: false),
                    Acceptor = table.Column<ulong>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountInvite", x => x.Token);
                    table.ForeignKey(
                        name: "FK_AccountInvite_Accounts_Acceptor",
                        column: x => x.Acceptor,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountInvite_Accounts_Inviter",
                        column: x => x.Inviter,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "AccountPosts",
                columns: table => new
                {
                    Account = table.Column<ulong>(nullable: false),
                    Id = table.Column<uint>(nullable: false),
                    Content = table.Column<string>(type: "TEXT", maxLength: 65535, nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    Visibility = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountPosts", x => new { x.Id, x.Account });
                    table.ForeignKey(
                        name: "FK_AccountPosts_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Albums",
                columns: table => new
                {
                    Id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Account = table.Column<ulong>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Albums", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Albums_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Blogs",
                columns: table => new
                {
                    Id = table.Column<uint>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 48, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Creator = table.Column<ulong>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Visibility = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Blogs_Accounts_Creator",
                        column: x => x.Creator,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Account = table.Column<ulong>(nullable: false),
                    Content = table.Column<string>(maxLength: 255, nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Picture",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Uploaded = table.Column<DateTime>(nullable: false),
                    Uploader = table.Column<ulong>(nullable: true),
                    State = table.Column<byte>(nullable: false),
                    StateChanged = table.Column<DateTime>(nullable: true),
                    Path = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Picture", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Picture_Accounts_Uploader",
                        column: x => x.Uploader,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Sessions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Account = table.Column<ulong>(nullable: true),
                    Browser = table.Column<byte>(nullable: false),
                    Platform = table.Column<byte>(nullable: false),
                    Issued = table.Column<DateTime>(nullable: false),
                    LastUse = table.Column<DateTime>(nullable: true),
                    Expires = table.Column<DateTime>(nullable: false),
                    Ip = table.Column<string>(maxLength: 45, nullable: true),
                    LastIp = table.Column<string>(maxLength: 45, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sessions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sessions_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "AccountPostLike",
                columns: table => new
                {
                    Liker = table.Column<ulong>(nullable: false),
                    Account = table.Column<ulong>(nullable: false),
                    Post = table.Column<uint>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Unliked = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountPostLike", x => new { x.Account, x.Post, x.Liker });
                    table.ForeignKey(
                        name: "FK_AccountPostLike_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountPostLike_Accounts_Liker",
                        column: x => x.Liker,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountPostLike_AccountPosts_Post_Account",
                        columns: x => new { x.Post, x.Account },
                        principalTable: "AccountPosts",
                        principalColumns: new[] { "Id", "Account" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AlbumLike",
                columns: table => new
                {
                    Like = table.Column<ulong>(nullable: false),
                    Id = table.Column<uint>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Unliked = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlbumLike", x => new { x.Id, x.Like });
                    table.ForeignKey(
                        name: "FK_AlbumLike_Albums_Id",
                        column: x => x.Id,
                        principalTable: "Albums",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AlbumLike_Accounts_Like",
                        column: x => x.Like,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BlogAccesses",
                columns: table => new
                {
                    Account = table.Column<ulong>(nullable: false),
                    Blog = table.Column<uint>(nullable: false),
                    Access = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogAccesses", x => new { x.Blog, x.Account });
                    table.ForeignKey(
                        name: "FK_BlogAccesses_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlogAccesses_Blogs_Blog",
                        column: x => x.Blog,
                        principalTable: "Blogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BlogPosts",
                columns: table => new
                {
                    Id = table.Column<uint>(nullable: false),
                    Blog = table.Column<uint>(nullable: false),
                    Account = table.Column<ulong>(nullable: true),
                    Content = table.Column<string>(type: "TEXT", maxLength: 65535, nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(maxLength: 128, nullable: false),
                    Visibility = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogPosts", x => new { x.Blog, x.Id });
                    table.ForeignKey(
                        name: "FK_BlogPosts_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlogPosts_Blogs_Blog",
                        column: x => x.Blog,
                        principalTable: "Blogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccountPostComments",
                columns: table => new
                {
                    CommentId = table.Column<ulong>(nullable: false),
                    Post = table.Column<uint>(nullable: false),
                    Account = table.Column<ulong>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountPostComments", x => new { x.CommentId, x.Account, x.Post });
                    table.ForeignKey(
                        name: "FK_AccountPostComments_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountPostComments_Comments_CommentId",
                        column: x => x.CommentId,
                        principalTable: "Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountPostComments_AccountPosts_Post_Account",
                        columns: x => new { x.Post, x.Account },
                        principalTable: "AccountPosts",
                        principalColumns: new[] { "Id", "Account" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CommentReplies",
                columns: table => new
                {
                    Comment = table.Column<ulong>(nullable: false),
                    Reply = table.Column<ulong>(nullable: false),
                    CommentId1 = table.Column<ulong>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommentReplies", x => new { x.Comment, x.Reply });
                    table.ForeignKey(
                        name: "FK_CommentReplies_Comments_Comment",
                        column: x => x.Comment,
                        principalTable: "Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CommentReplies_Comments_CommentId1",
                        column: x => x.CommentId1,
                        principalTable: "Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CommentReplies_Comments_Reply",
                        column: x => x.Reply,
                        principalTable: "Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccountPostPictures",
                columns: table => new
                {
                    Post = table.Column<uint>(nullable: false),
                    Account = table.Column<ulong>(nullable: false),
                    Picture = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountPostPictures", x => new { x.Account, x.Post, x.Picture });
                    table.ForeignKey(
                        name: "FK_AccountPostPictures_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountPostPictures_Picture_Picture",
                        column: x => x.Picture,
                        principalTable: "Picture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountPostPictures_AccountPosts_Post_Account",
                        columns: x => new { x.Post, x.Account },
                        principalTable: "AccountPosts",
                        principalColumns: new[] { "Id", "Account" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AlbumPictures",
                columns: table => new
                {
                    Id = table.Column<uint>(nullable: false),
                    Picture = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlbumPictures", x => new { x.Id, x.Picture });
                    table.ForeignKey(
                        name: "FK_AlbumPictures_Albums_Id",
                        column: x => x.Id,
                        principalTable: "Albums",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AlbumPictures_Picture_Picture",
                        column: x => x.Picture,
                        principalTable: "Picture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PictureLike",
                columns: table => new
                {
                    Picture = table.Column<Guid>(nullable: false),
                    Account = table.Column<ulong>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Unliked = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PictureLike", x => new { x.Picture, x.Account });
                    table.ForeignKey(
                        name: "FK_PictureLike_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PictureLike_Picture_Picture",
                        column: x => x.Picture,
                        principalTable: "Picture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BlogPostComments",
                columns: table => new
                {
                    Blog = table.Column<uint>(nullable: false),
                    Post = table.Column<uint>(nullable: false),
                    CommentId = table.Column<ulong>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogPostComments", x => new { x.CommentId, x.Post, x.Blog });
                    table.ForeignKey(
                        name: "FK_BlogPostComments_Blogs_Blog",
                        column: x => x.Blog,
                        principalTable: "Blogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlogPostComments_Comments_CommentId",
                        column: x => x.CommentId,
                        principalTable: "Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlogPostComments_BlogPosts_Blog_Post",
                        columns: x => new { x.Blog, x.Post },
                        principalTable: "BlogPosts",
                        principalColumns: new[] { "Blog", "Id" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "CanTarget", "Description", "Disabled", "Name" },
                values: new object[] { 1u, 1u, "Default admin role", false, "Admin" });

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "Id", "Bio", "BlockedDate", "Description", "FailedAuths", "FirstName", "Invite", "LastFailedAuth", "LastName", "Password", "Picture", "Email", "RegisteredDate", "Role", "ShowEmail", "ShowName", "ShowWebsite", "State", "Username", "Website" },
                values: new object[] { 1ul, null, new DateTime(2019, 12, 22, 13, 0, 45, 705, DateTimeKind.Utc).AddTicks(1563), "Default administrator account", (byte)0, "Admin", null, null, "Default", "", null, null, new DateTime(2019, 12, 22, 13, 0, 45, 705, DateTimeKind.Utc).AddTicks(1205), 1u, (byte)0, (byte)0, (byte)0, (byte)0, "administrator", null });

            migrationBuilder.InsertData(
                table: "RolePermissions",
                columns: new[] { "Role", "Permission", "CanTarget" },
                values: new object[] { 1u, "*", 1u });

            migrationBuilder.InsertData(
                table: "AccountEmails",
                columns: new[] { "Email", "Account", "Type", "Verified" },
                values: new object[] { "admin@domain.tld", 1ul, (byte)1, true });

            migrationBuilder.CreateIndex(
                name: "IX_AccountEmails_Account",
                table: "AccountEmails",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IX_AccountInvite_Acceptor",
                table: "AccountInvite",
                column: "Acceptor",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AccountInvite_Inviter",
                table: "AccountInvite",
                column: "Inviter");

            migrationBuilder.CreateIndex(
                name: "IX_AccountPostComments_Account",
                table: "AccountPostComments",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IX_AccountPostComments_CommentId",
                table: "AccountPostComments",
                column: "CommentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AccountPostComments_Post_Account",
                table: "AccountPostComments",
                columns: new[] { "Post", "Account" });

            migrationBuilder.CreateIndex(
                name: "IX_AccountPostLike_Liker",
                table: "AccountPostLike",
                column: "Liker");

            migrationBuilder.CreateIndex(
                name: "IX_AccountPostLike_Post_Account",
                table: "AccountPostLike",
                columns: new[] { "Post", "Account" });

            migrationBuilder.CreateIndex(
                name: "IX_AccountPostPictures_Picture",
                table: "AccountPostPictures",
                column: "Picture");

            migrationBuilder.CreateIndex(
                name: "IX_AccountPostPictures_Post_Account",
                table: "AccountPostPictures",
                columns: new[] { "Post", "Account" });

            migrationBuilder.CreateIndex(
                name: "IX_AccountPosts_Account",
                table: "AccountPosts",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Picture",
                table: "Accounts",
                column: "Picture");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Email",
                table: "Accounts",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Role",
                table: "Accounts",
                column: "Role");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Username",
                table: "Accounts",
                column: "Username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AlbumLike_Like",
                table: "AlbumLike",
                column: "Like");

            migrationBuilder.CreateIndex(
                name: "IX_AlbumPictures_Picture",
                table: "AlbumPictures",
                column: "Picture",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Albums_Account",
                table: "Albums",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IX_BlogAccesses_Account",
                table: "BlogAccesses",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IX_BlogPostComments_CommentId",
                table: "BlogPostComments",
                column: "CommentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BlogPostComments_Blog_Post",
                table: "BlogPostComments",
                columns: new[] { "Blog", "Post" });

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_Account",
                table: "BlogPosts",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IX_Blogs_Creator",
                table: "Blogs",
                column: "Creator");

            migrationBuilder.CreateIndex(
                name: "IX_CommentReplies_Comment",
                table: "CommentReplies",
                column: "Comment",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CommentReplies_CommentId1",
                table: "CommentReplies",
                column: "CommentId1");

            migrationBuilder.CreateIndex(
                name: "IX_CommentReplies_Reply",
                table: "CommentReplies",
                column: "Reply",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Comments_Account",
                table: "Comments",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IX_Picture_Uploader",
                table: "Picture",
                column: "Uploader");

            migrationBuilder.CreateIndex(
                name: "IX_PictureLike_Account",
                table: "PictureLike",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IX_RolePermissions_CanTarget",
                table: "RolePermissions",
                column: "CanTarget");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_CanTarget",
                table: "Roles",
                column: "CanTarget");

            migrationBuilder.CreateIndex(
                name: "IX_Sessions_Account",
                table: "Sessions",
                column: "Account");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_Picture_Picture",
                table: "Accounts",
                column: "Picture",
                principalTable: "Picture",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_AccountEmails_Email",
                table: "Accounts",
                column: "Email",
                principalTable: "AccountEmails",
                principalColumn: "Email",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountEmails_Accounts_Account",
                table: "AccountEmails");

            migrationBuilder.DropForeignKey(
                name: "FK_Picture_Accounts_Uploader",
                table: "Picture");

            migrationBuilder.DropTable(
                name: "AccountInvite");

            migrationBuilder.DropTable(
                name: "AccountPostComments");

            migrationBuilder.DropTable(
                name: "AccountPostLike");

            migrationBuilder.DropTable(
                name: "AccountPostPictures");

            migrationBuilder.DropTable(
                name: "AlbumLike");

            migrationBuilder.DropTable(
                name: "AlbumPictures");

            migrationBuilder.DropTable(
                name: "BlogAccesses");

            migrationBuilder.DropTable(
                name: "BlogPostComments");

            migrationBuilder.DropTable(
                name: "CommentReplies");

            migrationBuilder.DropTable(
                name: "PictureLike");

            migrationBuilder.DropTable(
                name: "RolePermissions");

            migrationBuilder.DropTable(
                name: "Sessions");

            migrationBuilder.DropTable(
                name: "AccountPosts");

            migrationBuilder.DropTable(
                name: "Albums");

            migrationBuilder.DropTable(
                name: "BlogPosts");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Blogs");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "Picture");

            migrationBuilder.DropTable(
                name: "AccountEmails");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}
