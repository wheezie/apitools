﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wheezie.ApiTools.EF.Migrations
{
    public partial class fixed_rolenullables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<uint>(
                name: "Role",
                table: "Accounts",
                nullable: false,
                defaultValue: 2u,
                oldClrType: typeof(uint),
                oldType: "int unsigned",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                columns: new[] { "RegisteredDate", "Role" },
                values: new object[] { new DateTime(2019, 12, 23, 19, 27, 31, 53, DateTimeKind.Utc).AddTicks(8233), 1u });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "CanTarget", "Description", "Disabled", "Name" },
                values: new object[] { 2u, null, "Default user role", false, "User" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2u);

            migrationBuilder.AlterColumn<uint>(
                name: "Role",
                table: "Accounts",
                type: "int unsigned",
                nullable: true,
                oldClrType: typeof(uint),
                oldDefaultValue: 2u);

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1ul,
                column: "RegisteredDate",
                value: new DateTime(2019, 12, 22, 17, 19, 23, 450, DateTimeKind.Utc).AddTicks(5502));
        }
    }
}
