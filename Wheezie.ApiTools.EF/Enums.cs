namespace Wheezie.ApiTools.EF
{
    public enum Visibility : byte
    {
        /// Public
        Public = 0,
        /// Friends only
        Friends = 1,
        /// Members only
        MembersOnly = 2,
        /// Not visibile
        None = 255
    }

    public enum Access : byte
    {
        Read,
        Moderate,
        Write,
        Full = 255
    }

    public enum Browser : byte
    {
        Unknown = 0,
        Chrome = 1,
        IE = 2,
        Safari = 4,
        Firefox = 8,
        Edge = 16,
        Opera = 32,
        Others = 64
    }

    public enum Platform : byte
    {
        Unknown = 0,
        Windows = 1,
        Mac = 2,
        iOS = 4,
        Linux = 8,
        Android = 16,
        Other = 32
    }
}