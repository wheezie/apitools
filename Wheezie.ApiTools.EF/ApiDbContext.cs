using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using Wheezie.ApiTools.EF.Models;
using Wheezie.ApiTools.EF.Models.Generic;

namespace Wheezie.ApiTools.EF
{
    public class AppDbContext : DbContext
    {
        // Account
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountEmail> AccountEmails { get; set; }
        public DbSet<AccountInvite> AccountInvite { get; set; }
        // Account posts
        public DbSet<AccountPost> AccountPosts { get; set; }
        public DbSet<AccountPostPicture> AccountPostPictures { get; set; }
        public DbSet<AccountPostComment> AccountPostComments { get; set; }

        // Album
        public DbSet<Album> Albums { get; set; }
        public DbSet<AlbumLike> AlbumLikes { get; set; }
        public DbSet<AlbumPicture> AlbumPictures { get; set; }

        // Blog
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<BlogAccess> BlogAccesses { get; set; }
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<BlogPostComment> BlogPostComments { get; set; }

        // Comment
        public DbSet<Comment> Comments { get; set; }
        public DbSet<CommentReply> CommentReplies { get; set; }

        // Event
        public DbSet<Event> Events { get; set; }
        public DbSet<EventCrew> EventCrews { get; set; }
        public DbSet<EventCrewMember> EventCrewMembers { get; set; }
        public DbSet<EventDate> EventDates { get; set; }

        // Role
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }

        // Pictures
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<PictureLike> PictureLikes { get; set; }

        // Session
        public DbSet<Session> Sessions { get; set; }


        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<AccessBase>();
            modelBuilder.Ignore<ContentBase>();
            modelBuilder.Ignore<LikeBase>();
            modelBuilder.Ignore<Post>();

#region Account
            modelBuilder.Entity<Account>(account =>
            {
                account.Property(a => a.Username)
                    .HasMaxLength(32)
                    .IsRequired()
                    .IsUnicode();

                account.Property(a => a.Password)
                    .HasMaxLength(60)
                    .IsFixedLength()
                    .IsRequired();

                account.Property(a => a.FirstName)
                    .HasMaxLength(32)
                    .IsUnicode();

                account.Property(a => a.LastName)
                    .HasMaxLength(32)
                    .IsUnicode();

                account.Ignore(a => a.FullName);

                account.Property(a => a.Description)
                    .HasMaxLength(1024)
                    .IsUnicode();

                account.Property(a => a.RoleId)
                    .HasColumnName("Role")
                    .IsRequired(true)
                    .HasDefaultValue(2);

                account.Property(a => a.PictureId)
                    .HasColumnName("Picture");

                account.Property(p => p.ProfileEmailId)
                    .HasColumnName("Email")
                    .HasMaxLength(255)
                    .IsUnicode();

                account.Property(p => p.Bio)
                    .HasMaxLength(255)
                    .IsUnicode();

                account.Property(p => p.Website)
                    .HasMaxLength(255)
                    .IsUnicode();

                account.HasOne(a => a.Picture)
                    .WithMany()
                    .HasForeignKey(a => a.PictureId);

                account.HasMany(a => a.Emails)
                    .WithOne(e => e.Account)
                    .HasForeignKey(e => e.AccountId)
                    .OnDelete(DeleteBehavior.SetNull);

                account.HasOne(a => a.ProfileEmail)
                    .WithOne()
                    .HasForeignKey<Account>(a => a.ProfileEmailId);

                account.HasOne(a => a.Invite)
                    .WithOne(i => i.Acceptor)
                    .HasForeignKey<AccountInvite>(i => i.AcceptorId)
                    .OnDelete(DeleteBehavior.Cascade);

                account.HasMany(a => a.Invites)
                    .WithOne(i => i.Inviter)
                    .HasForeignKey(i => i.InviterId)
                    .OnDelete(DeleteBehavior.SetNull);

                account.HasMany(a => a.Posts)
                    .WithOne(p => p.Account)
                    .HasForeignKey(p => p.AccountId)
                    .OnDelete(DeleteBehavior.Cascade);

                account.HasMany(a => a.Albums)
                    .WithOne(a => a.Account)
                    .HasForeignKey(a => a.AccountId)
                    .OnDelete(DeleteBehavior.SetNull);

                account.HasMany(a => a.BlogAccess)
                    .WithOne(b => b.Account)
                    .HasForeignKey(b => b.AccountId)
                    .OnDelete(DeleteBehavior.Cascade);

                account.HasOne(a => a.Role)
                    .WithMany(r => r.Members)
                    .HasForeignKey(a => a.RoleId)
                    .OnDelete(DeleteBehavior.Restrict);

                account.HasMany(a => a.Sessions)
                    .WithOne(s => s.Account)
                    .HasForeignKey(s => s.AccountId)
                    .OnDelete(DeleteBehavior.SetNull);

                account.HasIndex(a => a.Username)
                    .IsUnique();

                account.HasData(
                    new Account {
                        Id = 1,
                        Username = "administrator",
                        Description = "Default administrator account",
                        FirstName = "Admin",
                        LastName = "Default",
                        Password = "",
                        RoleId = 1u,
                        RegisteredDate = DateTime.MinValue,
                        State = AccountState.ConfirmedBlocked
                    }
                );
            });
            modelBuilder.Entity<AccountEmail>(email =>
            {
                email.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsRequired()
                    .IsUnicode();

                email.Property(e => e.AccountId)
                    .HasColumnName("Account");

                email.HasKey(e => e.Email);

                email.HasData(
                    new AccountEmail { AccountId = 1, Email = "admin@domain.tld", Type = EmailType.Primary, Verified = true }
                );
            });
            modelBuilder.Entity<AccountInvite>(invite =>
            {
                invite.Property(i => i.AcceptorId)
                    .HasColumnName("Acceptor")
                    .IsRequired(false);

                invite.Property(i => i.InviterId)
                    .HasColumnName("Inviter")
                    .IsRequired(false);

                invite.HasKey(i => i.Token);
            });
            modelBuilder.Entity<AccountPost>(post =>
            {
                post.HasBaseType((Type)null);

                post.Property(p => p.AccountId)
                    .HasColumnName("Account");

                post.Property(p => p.Title)
                    .HasMaxLength(100)
                    .IsRequired()
                    .IsUnicode();

                post.Property(p => p.Content)
                    .HasColumnType("TEXT")
                    .HasMaxLength(65535)
                    .IsRequired()
                    .IsUnicode();

                post.HasKey(p => new { p.Id, p.AccountId });

                post.HasMany(p => p.Comments)
                    .WithOne(c => c.Post)
                    .HasForeignKey(c => new { c.PostId, c.AccountId })
                    .OnDelete(DeleteBehavior.Cascade);

                post.HasMany(p => p.Pictures)
                    .WithOne(p => p.Post)
                    .HasForeignKey(p => new { p.PostId, p.AccountId })
                    .OnDelete(DeleteBehavior.Cascade);

                post.HasMany(p => p.Likes)
                    .WithOne(p => p.Post)
                    .HasForeignKey(p => new { p.PostId, p.AccountId });
            });
            modelBuilder.Entity<AccountPostComment>(comment =>
            {
                comment.Property(c => c.PostId)
                    .HasColumnName("Post")
                    .IsRequired();

                comment.Property(c => c.AccountId)
                    .HasColumnName("Account")
                    .IsRequired();

                comment.HasKey(c => new { c.CommentId, c.AccountId, c.PostId });

                comment.HasOne(c => c.Account)
                    .WithMany()
                    .HasForeignKey(c => c.AccountId);

                comment.HasOne(c => c.Comment)
                    .WithOne()
                    .HasForeignKey<AccountPostComment>(c => c.CommentId);
            });
            modelBuilder.Entity<AccountPostLike>(like =>
            {
                like.Property(l => l.PostId)
                    .HasColumnName("Post");

                like.Property(l => l.AccountId)
                    .HasColumnName("Account");

                like.Property(l => l.LikerId)
                    .HasColumnName("Liker");

                like.HasKey(l => new { l.AccountId, l.PostId, l.LikerId });

                like.HasOne(l => l.Account)
                    .WithMany()
                    .HasForeignKey(l => l.AccountId)
                    .OnDelete(DeleteBehavior.Cascade);

                like.HasOne(l => l.Liker)
                    .WithMany()
                    .HasForeignKey(l => l.LikerId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
            modelBuilder.Entity<AccountPostPicture>(picture =>
            {
                picture.Property(p => p.AccountId)
                    .HasColumnName("Account");

                picture.Property(p => p.PostId)
                    .HasColumnName("Post");

                picture.Property(p => p.PictureId)
                    .HasColumnName("Picture");

                picture.HasKey(p => new { p.AccountId, p.PostId, p.PictureId });

                picture.HasOne(p => p.Account)
                    .WithMany()
                    .HasForeignKey(p => p.AccountId)
                    .OnDelete(DeleteBehavior.Cascade);

                picture.HasOne(p => p.Picture)
                    .WithMany()
                    .HasForeignKey(p => p.PictureId)
                    .OnDelete(DeleteBehavior.ClientCascade);
            });
#endregion
#region Album
            modelBuilder.Entity<Album>(album =>
            {
                album.Property(a => a.AccountId)
                    .HasColumnName("Account");

                album.Property(a => a.Name)
                    .HasMaxLength(48)
                    .IsRequired();

                album.Property(a => a.Description)
                    .HasMaxLength(255)
                    .IsRequired(false);

                album.HasOne(a => a.Account)
                    .WithMany(a => a.Albums)
                    .HasForeignKey(a => a.AccountId)
                    .OnDelete(DeleteBehavior.SetNull);

                album.HasMany(a => a.Pictures)
                    .WithOne(p => p.Album)
                    .HasForeignKey(p => p.Id)
                    .OnDelete(DeleteBehavior.Cascade);

                album.HasMany(a => a.Likes)
                    .WithOne(l => l.Album)
                    .HasForeignKey(l => l.Id)
                    .OnDelete(DeleteBehavior.Cascade);
            });
            modelBuilder.Entity<AlbumLike>(like =>
            {
                like.Property(l => l.LikerId)
                    .HasColumnName("Like");

                like.HasKey(l => new { l.Id, l.LikerId });

                like.HasOne(l => l.Liker)
                    .WithMany()
                    .HasForeignKey(l => l.LikerId);
            });
            modelBuilder.Entity<AlbumPicture>(picture =>
            {
                picture.Property(p => p.PictureId)
                    .HasColumnName("Picture");

                picture.HasKey(p => new { p.Id, p.PictureId });

                picture.HasOne(p => p.Picture)
                    .WithOne()
                    .HasForeignKey<AlbumPicture>(p => p.PictureId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
#endregion
#region Blog
            modelBuilder.Entity<Blog>(blog =>
            {
                blog.Property(b => b.Name)
                    .HasMaxLength(48)
                    .IsRequired()
                    .IsUnicode();

                blog.Property(b => b.Description)
                    .HasMaxLength(255)
                    .IsUnicode();

                blog.Property(b => b.CreatorId)
                    .HasColumnName("Creator");

                blog.HasOne(b => b.Creator)
                    .WithMany(a => a.Blogs)
                    .HasForeignKey(b => b.CreatorId)
                    .OnDelete(DeleteBehavior.SetNull);

                blog.HasMany(b => b.Access)
                    .WithOne(a => a.Blog)
                    .HasForeignKey(a => a.BlogId);

                blog.HasMany(b => b.Posts)
                    .WithOne(p => p.Blog)
                    .HasForeignKey(p => p.BlogId);
            });
            modelBuilder.Entity<BlogAccess>(access =>
            {
                access.Property(a => a.AccountId)
                    .HasColumnName("Account");

                access.Property(a => a.BlogId)
                    .HasColumnName("Blog");

                access.HasKey(a => new { a.BlogId, a.AccountId });
            });
            modelBuilder.Entity<BlogPost>(post =>
            {
                post.HasBaseType((Type)null);

                post.Property(b => b.BlogId)
                    .HasColumnName("Blog");

                post.Property(p => p.AccountId)
                    .HasColumnName("Account");

                post.Property(p => p.Title)
                    .HasMaxLength(128)
                    .IsRequired()
                    .IsUnicode();

                post.Property(p => p.Content)
                    .HasColumnType("TEXT")
                    .HasMaxLength(65535)
                    .IsRequired()
                    .IsUnicode();

                post.HasKey(p => new { p.BlogId, p.Id });

                post.HasMany(p => p.Comments)
                    .WithOne(c => c.Post)
                    .HasForeignKey(c => new { c.BlogId, c.PostId } );
            });
            modelBuilder.Entity<BlogPostComment>(comment =>
            {
                comment.Property(c => c.BlogId)
                    .HasColumnName("Blog")
                    .IsRequired();
                comment.Property(c => c.PostId)
                    .HasColumnName("Post")
                    .IsRequired();

                comment.HasKey(c => new { c.CommentId, c.PostId, c.BlogId });

                comment.HasOne(c => c.Comment)
                    .WithOne()
                    .HasForeignKey<BlogPostComment>(c => c.CommentId);
            });
#endregion
#region Comment
            modelBuilder.Entity<Comment>(comment =>
            {
                comment.Property(c => c.Content)
                    .HasMaxLength(255)
                    .IsRequired()
                    .IsUnicode();

                comment.Property(c => c.AccountId)
                    .HasColumnName("Account")
                    .IsRequired();

                comment.HasOne(c => c.Account)
                    .WithMany()
                    .HasForeignKey(c => c.AccountId);

                comment.HasMany(c => c.Replies)
                    .WithOne(r => r.Comment)
                    .HasForeignKey(r => r.CommentId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
            modelBuilder.Entity<CommentReply>(reply =>
            {
                reply.Property(r => r.ReplyId)
                    .HasColumnName("Reply");

                reply.Property(r => r.CommentId)
                    .HasColumnName("Comment");

                reply.HasKey(r => new { r.CommentId, r.ReplyId });

                reply.HasOne(r => r.Reply)
                    .WithOne()
                    .HasForeignKey<CommentReply>(r => r.ReplyId);

                reply.HasOne(r => r.Comment)
                    .WithOne()
                    .HasForeignKey<CommentReply>(r => r.CommentId);
            });
#endregion
#region Event
            modelBuilder.Entity<Event>(evnt =>
            {
                evnt.Property(e => e.Name)
                    .HasMaxLength(64)
                    .IsRequired()
                    .IsUnicode();

                evnt.Property(e => e.Description)
                    .HasMaxLength(65535)
                    .IsRequired()
                    .IsUnicode();

                evnt.Property(e => e.Promotion)
                    .HasMaxLength(255);

                evnt.HasMany(e => e.Dates)
                    .WithOne(d => d.Event)
                    .HasForeignKey(d => d.Id)
                    .OnDelete(DeleteBehavior.Cascade);

                evnt.HasMany(e => e.Crews)
                    .WithOne(c => c.Event)
                    .HasForeignKey(c => c.Id)
                    .OnDelete(DeleteBehavior.Cascade);

                evnt.HasOne(e => e.Account)
                    .WithMany()
                    .HasForeignKey(e => e.AccountId)
                    .OnDelete(DeleteBehavior.Restrict);
            });
            modelBuilder.Entity<EventDate>(date =>
            {
                date.HasKey(d => new { d.Id, d.Date });
            });
            modelBuilder.Entity<EventCrew>(crew =>
            {
                crew.Property(c => c.Name)
                    .HasMaxLength(32)
                    .IsRequired()
                    .IsUnicode();

                crew.HasKey(c => new { c.Id, c.CrewId });

                crew.HasMany(c => c.Members)
                    .WithOne(m => m.Crew)
                    .HasForeignKey(m => new { m.Id, m.CrewId });
            });
            modelBuilder.Entity<EventCrewMember>(member =>
            {
                member.Property(m => m.Title)
                    .HasMaxLength(32)
                    .IsRequired()
                    .IsUnicode();

                member.Property(m => m.Name)
                    .HasMaxLength(65)
                    .IsRequired()
                    .IsUnicode();

                member.HasAlternateKey(m => m.Title);

                member.HasOne(m => m.Event)
                    .WithMany()
                    .HasForeignKey(m => m.Id)
                    .OnDelete(DeleteBehavior.Cascade);
            });
#endregion
#region Picture
            modelBuilder.Entity<Picture>(picture =>
            {
                picture.Property(p => p.UploaderId)
                    .HasColumnName("Uploader");

                picture.Property(p => p.Uploaded);

                picture.HasOne(p => p.Uploader)
                    .WithMany()
                    .HasForeignKey(p => p.UploaderId)
                    .OnDelete(DeleteBehavior.ClientCascade);
            });
            modelBuilder.Entity<PictureLike>(like =>
            {
                like.Property(l => l.AccountId)
                    .HasColumnName("Account");

                like.Property(l => l.PictureId)
                    .HasColumnName("Picture");

                like.HasKey(l => new { l.PictureId, l.AccountId });

                like.HasOne(l => l.Picture)
                    .WithMany()
                    .HasForeignKey(l => l.PictureId)
                    .OnDelete(DeleteBehavior.Cascade);

                like.HasOne(l => l.Account)
                    .WithMany()
                    .HasForeignKey(l => l.AccountId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
#endregion
#region Role
            modelBuilder.Entity<Role>(role =>
            {
                role.Property(r => r.Name)
                    .HasMaxLength(32)
                    .IsRequired();

                role.Property(r => r.Description)
                    .HasMaxLength(512);

                role.Property(r => r.CanTargetId)
                    .HasColumnName("CanTarget");

                role.HasOne(r => r.CanTarget)
                    .WithMany()
                    .HasForeignKey(r => r.CanTargetId);

                role.HasMany(r => r.Permissions)
                    .WithOne(p => p.Role)
                    .HasForeignKey(p => p.RoleId)
                    .OnDelete(DeleteBehavior.ClientCascade);

                role.HasMany(r => r.Members)
                    .WithOne(a => a.Role)
                    .HasForeignKey(a => a.RoleId)
                    .OnDelete(DeleteBehavior.Restrict);

                role.HasData(
                    new Role { Id = 1, CanTargetId = 1, Name = "Admin", Description = "Default admin role", Disabled = false },
                    new Role { Id = 2, CanTargetId = null, Name = "User", Description = "Default user role", Disabled = false }
                );
            });
            modelBuilder.Entity<RolePermission>(perm =>
            {
                perm.Property(p => p.RoleId)
                    .HasColumnName("Role");
                perm.Property(p => p.CanTargetId)
                    .HasColumnName("CanTarget");

                perm.HasKey(p => new { p.RoleId, p.Permission });

                perm.HasOne(p => p.CanTarget)
                    .WithMany()
                    .HasForeignKey(p => p.CanTargetId)
                    .OnDelete(DeleteBehavior.Cascade);

                perm.HasData(
                    new RolePermission { RoleId = 1, Permission = "*", CanTargetId = 2 }
                );
            });
#endregion
#region Session
            modelBuilder.Entity<Session>(session =>
            {
                session.Property(a => a.AccountId)
                    .HasColumnName("Account")
                    .IsRequired(false);

                session.Property(a => a.Ip)
                    .HasMaxLength(45);

                session.Property(a => a.LastIp)
                    .HasMaxLength(45);
            });
#endregion

            /* Current hack to set all datebase-related datetimes to UTC */
            var dateTimeConverter = new ValueConverter<DateTime, DateTime>(
                v => v, v => DateTime.SpecifyKind(v, DateTimeKind.Utc));

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
                foreach (var property in entityType.GetProperties())
                    if (property.ClrType == typeof(DateTime) || property.ClrType == typeof(DateTime?))
                        property.SetValueConverter(dateTimeConverter);
        }
    }
}